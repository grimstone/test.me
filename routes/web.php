<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'HomeController@index');
Route::get('/import/{id}', 'ApiController@import');

Route::group(['middleware' => ['web','auth']], function () {
	Route::get('/edit/task/{id}', 'TasksController@edit');
	Route::get('/history/{id}', 'TasksController@history');
	Route::get('/network/deletesite/{id}', 'TasksController@delete');
	Route::get('/network/finishTest', 'TasksController@finishTest');
	Route::get('/network/reTest', 'TasksController@retest');
	Route::get('/network/ready', 'TasksController@ready');
	Route::get('/network/complete', 'TasksController@complete');
	Route::post('/network/pushsite', 'TasksController@push');
	Route::get('/create-pdf/{id}', 'TasksController@pdf');
	Route::get('/view-pdf/{id}', 'TasksController@viewPDF');
	/*Сообщение в Слак*/
	Route::get('/message', 'TasksController@slackMessage');
	/*ФИЛЬТРЫ*/
	Route::get('/{filter}', 'HomeController@index');
	
	Route::get('/network/search', 'TasksController@search');
	Route::get('search/results/{query?}', 'TasksController@search2');
	/*Отправка сообщения об ошибках верстальщику*/
	Route::get('/network/report-verstalshik', 'TasksController@errorReport');

	/*Комментарий*/
	Route::get('/network/sendComm', 'TasksController@sendComm');
	Route::post('/network/upload', 'TasksController@upload');

	/*Миньон доделал задание*/
	Route::get('/network/vseSdelanoGospodin/', 'TasksController@vseSdelanoGospodin');

	Route::get('add-site', function() {
		return view('add-site');
	});
});

/*Роуты для авторизации*/
Auth::routes();


/*Просмотр логов*/
Route::get('log/logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');

