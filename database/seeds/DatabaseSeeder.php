<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder {

  public function run()
  {
    $this->call('StatusTableSeeder');

    $this->command->info('Таблица статусов загружена данными!');
  }

}

class UserTableSeeder extends Seeder {

  public function run()
  {
    //DB::table('users')->delete();

    Status::create(['status' => 'Не проверено']);
  }

}
