-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1
-- Время создания: Ноя 22 2016 г., 13:45
-- Версия сервера: 10.1.16-MariaDB
-- Версия PHP: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `testme`
--

-- --------------------------------------------------------

--
-- Структура таблицы `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_11_02_135334_create_tasks_table', 2),
(4, '2016_11_03_154200_create_reports_table', 3);

-- --------------------------------------------------------

--
-- Структура таблицы `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `reports`
--

CREATE TABLE `reports` (
  `id` int(10) UNSIGNED NOT NULL,
  `site_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `action` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `author` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `reports`
--

INSERT INTO `reports` (`id`, `site_id`, `action`, `author`, `created_at`, `updated_at`) VALUES
(6, '56', 'Задача добавлена.', 'vbugakov', '2016-11-21 08:51:00', '2016-11-21 08:51:00'),
(7, '57', 'Задача добавлена.', 'vbugakov', '2016-11-21 15:30:25', '2016-11-21 15:30:25'),
(8, '58', 'Задача добавлена.', 'dmaleev', '2016-11-21 15:39:40', '2016-11-21 15:39:40'),
(9, '59', 'Задача добавлена.', 'dmaleev', '2016-11-22 09:03:41', '2016-11-22 09:03:41');

-- --------------------------------------------------------

--
-- Структура таблицы `tasks`
--

CREATE TABLE `tasks` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `domen` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `poddomen` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `blank` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `author` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci DEFAULT '0',
  `errors` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `tasks`
--

INSERT INTO `tasks` (`id`, `title`, `domen`, `poddomen`, `type`, `blank`, `author`, `status`, `errors`, `created_at`, `updated_at`) VALUES
(53, 'Видеонаблюдение', 'videonablyudenie.ru', 'видеонаблюдение.цитронмедиа.ру', 'Бизнес', '', 'vbugakov', '2', '\n							\n															\n							\n															\n							<!--<ol>\n								<li>Божечки, ничего не работает!</li>\n							</ol>-->\n							\n							<!--<ol>\n								<li>Божечки, ничего не работает!</li>\n							</ol>-->\n							\n							<!--<ol>\n								<li>Божечки, ничего не работает!</li>\n							</ol>-->\n							\n							<ol>\n								<li>Божечки, ничего не работает!</li><li>12312321</li><li>123123</li><li>123123</li><li><br></li>\n							</ol>\n						\n						\n						\n						\n													\n													', '2016-11-03 12:39:02', '2016-11-22 09:01:47'),
(58, 'Элькортиджо', 'elcortijo.ru', 'elcortijo.ru', 'Сайт', 'http://oracle.ru/blanks/docs/22233.html ', 'dmaleev', '3', '\n							\n															\n							\n															<ol>\n									<li>Почта</li><li>Закругления</li><li>Несоответствие модулей!</li>\n								</ol>\n													\n													', '2016-11-21 15:39:40', '2016-11-22 08:43:13'),
(59, 'Фишстайл трейд', 'фишстайл.ру', 'фишстайл.цинтромедиа.рф', 'Сайт', 'http://oracle.ru/blanks/docs/21988.html ', 'dmaleev', '3', NULL, '2016-11-22 09:03:41', '2016-11-22 09:04:11');

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `authName` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `authSurname` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `role` varchar(20) CHARACTER SET utf8 NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `name`, `authName`, `authSurname`, `role`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'vbugakov', 'Владимир', 'Бугаков', 'bugseeker', 'hassler@mail.ru', '$2y$10$5FJKKNjs/oRifQ1RH9NP4.gVupS7zJeCtFHyGHq2Fj9TmtqqT1p6S', 'IqW7avRhM2qPCqAUod1OnJsQrNVXYp3cfzu69kBBHb04MO64gmIJIjL9edS7', '2016-11-02 08:31:14', '2016-11-22 09:02:44'),
(2, 'dmaleev', 'Дмитрий', 'Малеев', 'coordinator', 'test@mail.test', '$2a$04$FvWFyuhbHkgPPZHyVU3fg.WuKuGMvt1D6hjNG9fIKVEsqPmjSOT/y', 'WHthSrMAGJ28jhJA3SZOTdHLQRvkSmF5K2PpGbcyXhBiGJnzZWjz0gEn2esc', NULL, '2016-11-22 09:56:50'),
(3, 'mverh', 'Максим', 'Верховский', 'coordinator', 'verhovsky@mail.ru', '$2a$04$TN0qfgL70Fjh3ftykwbC.uZL1Z5wve9.dx3YvudQeKpEf8wi/tUsy', NULL, NULL, NULL),
(4, 'yuzukov', 'Юрий', 'Зюков', 'bugseeker', '12323@mail.ru', '$2a$04$8yR3A3rxbCajTFEFGvgd8OFqSweSvnZOcwk2z8meVhpdpb6G/tVZS', NULL, '2016-11-21 21:00:00', '2016-11-21 21:00:00'),
(5, 'apetrakov', 'Александр', 'Петраков', 'coordinator', 'petrakov@mail.ru', '$2a$04$eDbMl8BL8.tYADnkqHljCO05EmEx3FJF8D/LsX4qZFyjkBhNJcR8q', NULL, '2016-11-21 21:00:00', '2016-11-21 21:00:00');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Индексы таблицы `reports`
--
ALTER TABLE `reports`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `tasks`
--
ALTER TABLE `tasks`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT для таблицы `reports`
--
ALTER TABLE `reports`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT для таблицы `tasks`
--
ALTER TABLE `tasks`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=60;
--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
