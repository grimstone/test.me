@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Панель управления
                	<div style="display:inline-block; float:right; clear:right; margin-top: -3px;">
					<input   type="text" id="searchInput" name="search" placeholder="Поиск по заданиям">
					<a onclick="taskSearch()" class="btn btn-info" style="height: 30px;vertical-align: bottom;padding: 3px 4px 2px 4px;">
							   Искать
							</a>
					</div>
					
					<div id="searchRes">
						<ul id="resList">


						</ul>
					</div>

                </div>
                <div id="logs">
					
                </div>
                
                <div class="panel-body">
                
 				<div class="alert alert-info" style="text-align:left;">
				
						  
						  <img src="img/egreen.jpg" style=" float:left; margin:5px; clear:right; height:250px;" title="Это Ева Грин. И она недоумевает - из-за чего координаторы не видят задачи? Как и я, в принципе:3"/>
						  <h2>Господа координаторы и верстальщики, проверяйте видимость своих тасков, вроде должно работать.</h2>
						  <p>(но это не точно)</p>
						  <p style="text-decoration:underline;">Кроме этого, начал работать поиск!</p>
						  <div style="clear:both;"></div>

				</div>
				

				<div class="filter">
				    <div><a href="/">Все</a></div>
				    <div><a style="color:#3C763D;" href="/ready"><b>Только со статусом "Готово"</b></a></div>
					<div><a style="color:#3C763D;" href="/completed">Только со статусом "Сдано"</a></div>
					<div><a style="color:#A9A9A9;" href="/untested">Только со статусом "Не проверен"</a></div>
					<div><a style="color:#B84442;" href="/errors">Только ожидающие корректировки</a></div>
					<div><a style="color:#8A6D3B;" href="/waiting">Только ожидающие перепроверки</a></div>
				</div>
				<hr>



				<table class="table table-striped" id="tasks">
				<thead><tr><th>#</th><th>Название</th><th>Домен</th><th>Поддомен</th><th>Тип</th><th>Комплект</th><th>Координатор/Верстальщик</th><th>Статус</th><th>Действия</th></tr></thead>
				<tbody>
				@if(!empty($tasks))
					@if (isset($searchResults))
						$tasks = $searchResults
					@endif
					@foreach($tasks as $task)
				<tr>
				<td>{{ $task->userWorks['id'] }}</td>
				@if ($task->userWorks['status_id'] != 4)
					<td><a href="{{ $task->userWorks['blank'] }}" target="_blank"><img src="../img/invoice.png" title="Бланк заказа" /></a><a href="edit/task/{{$task->userWorks['id']}}">{{ $task->userWorks['name'] }}</a></td>
				@else
					<td><a href="{{ $task->userWorks['blank'] }}" target="_blank"><img src="../img/invoice.png" title="Бланк заказа" /></a> {{ $task->userWorks['name'] }} </td>
				@endif
				<td style="font-size:12px;">{{ $task->userWorks['domen'] }}</td>
				<td style="font-size:12px;">{{ $task->userWorks['poddomen'] }}</td>
				<td>
				{{ $task->userWorks['type'] }}
				</td>
				<td>
				{{ $task->userWorks['complect'] }}
				</td>
				<td>{{ $task->userWorks['author'] }}/{{ $task->userWorks['verst'] }}</td>
				@if($task->userWorks['status_id'] == 0)
					<td style="color:#A9A9A9; font-weight:bold;">Не проверен</td>
				@elseif($task->userWorks['status_id'] == 1)
					<td style="color:#B84442; font-weight:bold;">Проверен, есть ошибки</td>
				@elseif($task->userWorks['status_id'] == 2)
					<td style="color:#31708F; font-weight:bold;">Проверен, нет ошибок</td>
				@elseif($task->userWorks['status_id'] == 3)
					<td style="color:#8A6D3B; font-weight:bold;">Ждет перепроверки</td>
				@elseif($task->userWorks['status_id'] == 4)
					<td style="color:#3C763D; font-weight:bold;">Сдано</td>
				@endif
				<td>
				@if($isVerst == 1)
					<a href="/history/{{$task->userWorks['id']}}" title="Просмотр истории"><img class="task_icon" src="../img/task/history.png"/></a>
				@else

					@if ($task->userWorks['status_id'] != 4)

						@if ($statistic == 1 AND Auth::user()->id == 61) 
						<a style="cursor:pointer;" onclick="complete({{$task->userWorks['id']}}, {{$task->userWorks['status_id']}})" title="Проект сдан"><img class="task_icon" src="../img/task/complete.png"/></a>
						<!--elseif ($task->userWorks['type'] == 'Доработка' AND $statistic == 0)
						<a style="cursor:pointer;" onclick="complete({{$task->userWorks['id']}}, {{$task->userWorks['status_id']}})" title="Проект сдан"><img class="task_icon" src="../img/task/complete.png"/></a>-->
						@endif
						
						<a href="/history/{{$task->userWorks['id']}}" title="Просмотр истории"><img class="task_icon" src="../img/task/history.png"/></a> 
						
						<a href="/create-pdf/{{$task->userWorks['id']}}" title="Экспорт отчета"><img class="task_icon" src="../img/task/report.png"/></a> 
					
					
					@else
						<a title="Проект сдан"><img class="task_icon" src="../img/mark.png"/></a>
						<a href="/history/{{$task->userWorks['id']}}" title="Просмотр истории"><img class="task_icon" src="../img/task/history.png"/></a>
						<a href="/create-pdf/{{$task->userWorks['id']}}" title="Экспорт отчета"><img class="task_icon" src="../img/task/report.png"/></a> 
					@endif
				@endif
				@if ($statistic == 1)
					<!-- <a href="/network/deletesite/{{$task->userWorks['id']}}" title="Удалить из проверки"><img class="task_icon" src="../img/task/delete.png"/></a> -->
				@endif

				</td></tr>
				@endforeach
				
				@endif
				</tbody>
				</table>
				{{ $tasks->links() }}
				@if ($statistic == 1)
					<hr>

					<p>Статистика: <span>Заданий за сегодня: <b>{{$stat['today']}}</b></span>, <span style="color:#3C763D;">всего сдано: <b>{{$stat['completed']}}</b></span>, 
					<span style="color:#31708F;">всего ожидающих сдачи: <b>{{$stat['ready']}}</b></span>, <u style="color:#DD5044;">ожидающих проверки: <b>{{$stat['waiting']}}</b></u>, 
					<u style="color:#8A6D3B;">ожидающих перепроверки: <b>{{$stat['retest']}}</b></u></p>
				@else

				@endif
				<br>По вопросам и предложениям писать сюда: <a href="https://citron-media.slack.com/messages/@h4ssler/" target="_blank">клац</a>.
                </div>
            </div>
<!-- 			<a href="/add-site" class="btn btn-primary">
               Добавить сайт на проверку
            </a> -->
        </div>
    </div>
</div>
@endsection
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<script>
$(document).keypress(function(e) {
    if(e.which == 13) {
        taskSearch();
    }
});
function taskSearch() {
	query = $('#searchInput').val();

	document.location.href = 'search/results/'+query;

 	// 	$.get(
		// 	"search/results",
		// 		{
		// 			'query':		query,
		// 			'_token': 		'{!! csrf_token() !!}'
		// 		}
		// );
}

	function complete(id, status){
		console.log(status);
		if(status != 2){
			bootbox.alert("В проекте остались ошибки, или проект ожидает перепроверки", function() {
            });
		}else if(status == 2){
			$.get(
			"/network/complete/",
				{
					'id':			id,
					'_token': 		'{!! csrf_token() !!}'
				},
			onAjaxSuccessComplete
			);
		}
			
	}

	
	function onAjaxSuccessComplete(data){
		bootbox.alert("Проект сдан!", function() {
				$("#tasks").load("/ #tasks");
            });
	}

// function onSearchSuccess(data){
// 	$("#tasks").load("search/results #tasks")
// }
</script>

<script> $(document).ready(function(){
 
var url=document.location.href; $.each($(".filter a"),function(){
 
  if(this.href==url){$(this).addClass('active');};
 
  });
 
});
 
</script>