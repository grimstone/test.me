@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Добавить сайт на проверку</div>

                <div class="panel-body">
                    Заполните все поля
						<div class="alert alert-success" style="display:none;">
						  <strong>Успешно!</strong> Сайт добавлен на проверку.
						</div>
						<div class="alert alert-warning" style="display:none;">
						  <strong>Ошибка!</strong> Проверьте правильность введенных данных.
						</div>
					  <div class="form-group">
						<label for="siteTitle">Название сайта</label>
						<input type="text" class="form-control" id="siteTitle" name="siteTitle" placeholder="видеонаблюдение.ру" required>
					  </div>
					  <div class="form-group">
						<label for="domen">Домен (если имеется)</label>
						<input type="text" class="form-control" id="domen" name="domen" placeholder="видеонаблюдение.ру" required>
					  </div>
					  <div class="form-group">
						<label for="poddomen">Поддомен (если имеется)</label>
						<input type="text" class="form-control" id="poddomen" name="poddomen" placeholder="видеонаблюдение.ру" required>
					  </div>
					  <div class="form-group">
						<label for="blank">Бланк заказа:</label>
						<input type="text" class="form-control" id="blank" name="blank" placeholder="http://oracle.ru/..." required>
					  </div>
					  <div class="form-group">
						<label for="exampleInputPassword1">Тип</label>
						<select class="form-control" id="type" onchange="isFix(this)">
						  <option disabled selected>Выберите тип</option>
						  <option value="dorabotka">Доработка</option>
						  <option value="site">Сайт</option>
						</select>
					  </div>
					  <div id="fixprice" class="form-group" style="display:none;">
						<label for="price">Цена доработки:</label>
						<input type="text" class="form-control" id="price" name="price" placeholder="10 000 руб.">
					  </div>
					  <a onclick="pushSite()" class="btn btn-success">Добавить сайт на проверку</a>
					  <a onclick="history.back()" class="btn btn-warning">Назад</a>
                </div>
				
            </div>
        </div>
    </div>
</div>
@endsection

<script>
	
	function isFix(a){
		var type = a.value;
		if(type=='dorabotka'){
			$('#fixprice').show('slow');
		}else{
			$('#fixprice').hide('slow');
		}
	}
	
	
	function pushSite()
			{
				
				var name = $("input[name='siteTitle']").val();
				var type = $("#type :selected").text();
				var domen = $("input[name='domen']").val();
				var poddomen = $("input[name='poddomen']").val();
				var blank = $("input[name='blank']").val();
				var price = $("input[name='price']").val();
				
				if(name == '' || type == '' || type == 'Выберите тип'){
					$('.alert-warning').fadeIn(1000);
					$('.alert-warning').delay(3000).fadeOut(); 
					return false;
				}
				
				console.log(name);
				console.log(type);
				
				$.post(
					  "/network/pushsite",
					  {
						'name'      	: name,
						'type'    		: type,
						'domen'    		: domen,
						'poddomen'    	: poddomen,
						'blank'			: blank,
						'price'			: price,
						 '_token': '{!! csrf_token() !!}'
					  },
					  onAjaxSuccessPushSite
					);
			}

	function onAjaxSuccessPushSite(data)
			{
					if (data=='OK!'){
						$('.alert-success').fadeIn(1000);
						$('.alert-success').delay(3000).fadeOut(2000);
												
						setTimeout(function() {
							$(location).attr('href', '/')
						}, 3000);
					}
			}
</script>
