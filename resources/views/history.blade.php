@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">История изменений</div>
			
                <div class="panel-body">
				<table class="table table-striped" id="tasks">
				<thead><th>Дата</th><th>Действие</th><th>Автор</th><th>Ошибки</th></thead>
				<tbody>
				@if(!empty($reports))
				@foreach($reports as $report)
				<tr>		
				<td>{{ $report->updated_at }}</td>
				<td>{{ $report->action }}</td>
				<td>{{ $report->author }}</td>
                <td>{!! $report->errors !!}</td>
				</tr>
				@endforeach
				@endif
				</tbody>
				</table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
