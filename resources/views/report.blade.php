<html>
<title>{{ config('app.name') }} - Отчет по проверке сайта {{ $data['site'] }}</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />

<body style="padding:20px">
<h1 style="text-align:center;">Проверка качества выполненных работ группой разработки</h1>
<p style="text-align:right;">Сайт: {{ $data['site'] }}</p>
<p style="text-align:right;">Домен: {{ $data['domen'] }}</p>
<p style="text-align:right;">Тип: {{ $data['type'] }}</p>
<p style="text-align:right;">Комплект: {{ $data['complect'] }}</p>
<p style="text-align:right;">Бланк заказа: {{ $data['blank'] }}</p>
<p style="text-align:right;">Программист-тестировщик: Бугаков В. В.</p>
<p style="text-align:right;">Дата проверки: {{ $data['created_at']->format('d.m.Y') }}</p>
<h1>Найденные ошибки:</h1>
{!! $data['errors'] !!}
<div style="position:absolute; bottom:0px; font-size:14px; right:20px;">
<table style="width:100%;">
@if($data['type'] !='Доработка')
<tr>
<td>Несоответствие модулей (если имеется) согласовано с клиентом, координатор:</td>
<td>{{ $data['coordinator'] }}   /</td>
</tr>
@else

@endif

<tr>
<td>Корректность проверки подтверждаю, проверяющий:</td>
<td>Бугаков В.В.   /</td>
</tr>

<tr>
<td>Корректность проверки подтверждаю, руководитель группы разработки:</td>
<td>Зюков Ю.А.   /</td>
</tr>

</table>


</div>
</body>

</html>