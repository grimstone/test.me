@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Панель управления
                	<div style="display:inline-block; float:right; clear:right; margin-top: -3px;">
					<input   type="text" id="searchInput" name="search" placeholder="Поиск по заданиям">
					<a onclick="taskSearch()" class="btn btn-info" style="height: 30px;vertical-align: bottom;padding: 3px 4px 2px 4px;">
							   Искать
							</a>
					</div>
                <div class="panel-body">
                <h2>Результаты поиска</h2>
                @if(count($searchResults) == 0)
                <div style="text-align:center;">
                	<img src="/img/travolta.gif" alt="Ничего не найдено" title="Ничего не найдено"/>
                </div>
                @else
                <table class="table table-striped" id="tasks">
				<thead><tr><th>#</th><th>Название</th><th>Тип</th><th>Адрес</th><th>Бланк заказа</th></tr></thead>
				<tbody>
			@if(!empty($searchResults))
			@foreach($searchResults as $task)
				@if($task['status'] == 4 AND !Auth::user()->is_checker())
					<tr class="search_res_done">
				@else
					<tr class="search_res" onclick="window.location.href='/edit/task/{{ $task['id'] }}'";>
				@endif
				<td>{{ $task['id'] }}</td>
				<td style="font-size:12px;">{{ $task['site'] }}</td>
				<td style="font-size:12px;">{{ $task['type'] }}</td>
				<td style="font-size:12px;">{{ $task['site'] }}</td>
				@if($task['status'] == 4)
				<td style="font-size:12px;">{{ $task['blank'] }}</td>
				<td style="font-size:12px;"><u><b>√ СДАН</b></u></td>
				@else
				<td colspan="2" style="font-size:12px;">{{ $task['blank'] }}</td>
				</tr>
				@endif
				@endforeach
				@endif
				</tbody>
				</table>
				@endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<script>
$(document).keypress(function(e) {
    if(e.which == 13) {
        taskSearch();
    }
});
function taskSearch() {
	query = $('#searchInput').val();

	document.location.href = '/search/results/'+query;

 	// 	$.get(
		// 	"search/results",
		// 		{
		// 			'query':		query,
		// 			'_token': 		'{!! csrf_token() !!}'
		// 		}
		// );
}
</script>