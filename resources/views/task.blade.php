@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading" style="display:inline-block;">Проверка сайта <b>"{{ $sitename}}"</b></div>
				<div class="panel-heading" style="display:inline-block; float:right; clear:right;">Координатор: {{ $user}}<b></b></div>
                <div class="panel-body" style="min-height:600px;">
				<div class="block-inline">
						<h2 style="cursor:pointer" onclick="togMe(this)"><span class="caret"></span> Проверка верстки <img src="/img/browsers.png" title="Браузеры"/></h2>
						<div style="text-align:left;" class="client-side">
							<div class="panel panel-success front"> 
								<div class="panel-heading"> 
									<h3 class="panel-title">Кнопки и меню <b>(New!)</b></h3> 
								</div> 
								
								<div class="panel-body">
									<ul>
										<li class="shadow">При нажатии и удержании на кнопках и меню - на них не должно отображаться тени.</li>  
									</ul>
								
								</div> 
							</div>	

							<div class="panel panel-success front"> 
								<div class="panel-heading"> 
									<h3 class="panel-title">Поиск <b>(New!)</b></h3> 
								</div> 
								
								<div class="panel-body">
									<ul>
										<li>Пытаемся найти что-нибудь в поиске, переходим в любой раздел, после этого жмем кнопку назад - 
										ошибки с отправкой формы быть не должно.</li>
									</ul>
								
								</div> 
							</div>	

							<div class="panel panel-primary front"> 
								<div class="panel-heading"> 
									<h3 class="panel-title">Ссылки</h3> 
								</div> 
								
								<div class="panel-body"> 
									<ul>
										<li>Ссылки выделяются <u>подчеркиванием</u> </li>
										<li>Ссылки имеют 2 состояния (выделяются оттенком цвета):</li>
											<ul>
												<li><u>Обычное</u></li>
												<li><a class="hover-ex">Наведение</a></li>
											</ul>
									</ul>
								
								</div> 
							</div>
							<div class="panel panel-info front"> 
								<div class="panel-heading"> 
									<h3 class="panel-title">Шапка</h3> 
								</div> 
								
								<div class="panel-body"> 
									<ul>
										<li>Логотип ссылкой на главную, на всех страницах кроме самой главной</u> </li>
										<li>Логотип <span class="blurry-text">не размыт</span>, если клиент не пожелал обратного</li>
										<li>Наименование элементов <span style="color:red;"><abbr title="например, надпись «телефон» отличается от «+79066666666»">отличается</abbr></span> цветом от самих элементов</li>
										<li>В случае наличия времени работы, но отсутствия заголовка «время работы» надпись с днями недели должны отличаться от написания времени работы</li>										
									</ul>
								
								</div> 
							</div>
							<div class="panel panel-primary front"> 
								<div class="panel-heading"> 
									<h3 class="panel-title">Футер</h3> 
								</div> 
								
								<div class="panel-body"> 
									<ul>
										<li>Наименование элементов <span style="color:red;"><abbr title="например, надпись «телефон» отличается от «+79066666666»">отличается</abbr></span> цветом от самих элементов</li>
										<li>В случае наличия времени работы, но отсутствия заголовка «время работы» надпись с днями недели должны отличаться от написания времени работы</li>
									</ul>
								
								</div> 
							</div>
							<div class="panel panel-info front"> 
								<div class="panel-heading"> 
									<h3 class="panel-title">Меню</h3> 
								</div> 
								
								<div class="panel-body"> 
									<ul>
										<li>Пункты имеют 3 состояния </li>
											<ul>
												<li>Обычное</li>
												<li class="hover-ex">При наведении</li>
												<li><span class="active-ex">Активный раздел</span></li>		
											</ul>
										<li>Активные пункты\подпункты не подчеркиваются</li>
										<li>Активные разделы не кликаются</li>
										<li class="illusion">Без оптических иллюзий</li>										
									</ul>
								
								</div> 
							</div>
							<div class="panel panel-primary front"> 
								<div class="panel-heading"> 
									<h3 class="panel-title">Каталог</h3> 
								</div> 
								
								<div class="panel-body"> 
									<ul>
										<li><a class="addCart">Информирование</a> о добавлении в корзину</li>
										<li><a style="cursor:pointer;" onclick="rateUp()">Информирование</a> об учете голоса в рейтинге товара <span class="label label-success" style="display:none;">Ваш голос учтен!</span></li>
										<li>Активная страница постраничной навигации <u>не кликается</u></li>
										<li>Сортировка работает корректно</li>
									</ul>
								
								</div> 
							</div>
							<div class="panel panel-info front"> 
								<div class="panel-heading"> 
									<h3 class="panel-title">Сравнение</h3> 
								</div> 
								
								<div class="panel-body"> 
									<ul>
										<li>Нельзя сравнить 1 товар, при попытке выводится сообщение "Выберите еще один товар для сравнения"</u> </li>
										<li>Параметры в сравнении по бланку (без лишних)</li>										
									</ul>
								
								</div> 
							</div>
							<div class="panel panel-primary front"> 
								<div class="panel-heading"> 
									<h3 class="panel-title">Мини-корзина (в шапке)</h3> 
								</div> 
								
								<div class="panel-body"> 
									<ul>
										<li>Пустая корзина некликабельна, нет эффектов при наведении</li>
										<li>Количество товаров нормально отображается (до 100)</li>
									</ul>
								
								</div> 
							</div>
							<div class="panel panel-info front"> 
								<div class="panel-heading"> 
									<h3 class="panel-title">Формы</h3> 
								</div> 
								
								<div class="panel-body"> 
									<ul>
										<li><a style="cursor:pointer;" onclick="fieldIsNull()">Сообщение</a> о невозможности отправки формы с незаполненными обязательными полями <span class="label label-danger" style="display:none;">Заполните обязательные поля!</span></li>
									</ul>
								
								</div> 
							</div>
							<div class="panel panel-primary front"> 
								<div class="panel-heading"> 
									<h3 class="panel-title">Поиск</h3> 
								</div> 
								
								<div class="panel-body"> 
									<ul>
										<li><span class="zoom">"Лупа"</span> при наведении меняет цвет</li>
										<li>Пустой поиск выводит сообщение «По запросу ничего не найдено»</li>
										<li>Аякс подсказки по поиску не перекрывают сам поиск</li>
										<li>Если на сайте есть товары – они высвечиваются в аякс подсказках поиска</li>
									</ul>
								
								</div> 
							</div>
							<div class="panel panel-info front"> 
								<div class="panel-heading"> 
									<h3 class="panel-title">Опрос</h3> 
								</div> 
								
								<div class="panel-body"> 
									<ul>
										<li>Проверить работоспособность</li>
										<li><span style="color:#5CB85C;">Результаты</span> <span style="color:#D9534F;">выделены</span> <span style="color:#F0AD4E;">разным</span> <span style="color:#5BC0DE;">цветом</span></li>
									</ul>
								
								</div> 
							</div>							
							<div class="panel panel-primary front"> 
								<div class="panel-heading"> 
									<h3 class="panel-title">Новости</h3> 
								</div> 
								
								<div class="panel-body"> 
									<ul>
										<li>Надпись «опубликовано» не должна быть выделена, после неё не должно стоять «:»</li>
										<li>Дата публикации в списке новостей выделяется <span class="active-ex">прямоугольником</span> как в списке новостей боковой панели</li>
									</ul>
								
								</div> 
							</div>
							
							<div class="panel panel-info front"> 
								<div class="panel-heading"> 
									<h3 class="panel-title">Хлебные крошки</h3> 
								</div> 
								
								<div class="panel-body">
									<hr>
									<ul>
										<li>Над ними идет полоска, имеющая равные отступы над и под собой</li>
									</ul>
								
								</div> 
							</div>
							<div class="panel panel-primary front"> 
								<div class="panel-heading"> 
									<h3 class="panel-title">Демонаполнение</h3> 
								</div> 
								
								<div class="panel-body">
									<ul>
										<li>Раздел «О компании» - текст о нашей компании, сверху фото в 2 ряда по 3 шт</li>
										<li>Раздел контакты – Фото нашего здания, карта с указанием нашего здани, форма обратной связи</li>
										<li>Не должно быть <s>текста про яндекс</s></li>
									</ul>
								
								</div> 
							</div>
							<div class="panel panel-info front"> 
								<div class="panel-heading"> 
									<h3 class="panel-title">Копирайт Цитрон</h3> 
								</div> 
								
								<div class="panel-body">
									<ul>
										<li>При наведении становится <span class="colorful">цветным</span></li>
										<li><abbr title="Создание сайта – центр интернет-разработок «Цитрон»">Всплывающая подсказка</abbr></li>
									</ul>
								
								</div> 
							</div>								
							<div class="panel panel-primary front"> 
								<div class="panel-heading"> 
									<h3 class="panel-title">В целом по сайту</h3> 
								</div> 
								
								<div class="panel-body">
									<ul>
										<li class="to-right">Контент не прилипает к правому/левому блоку сайта</li>
										<li>Углы или прямые или скругленные</li>
										<li>Фон грузится после загрузки сайта</li>
									</ul>
								
								</div> 
							</div>								
						</div>
				</div>
				
				<div class="block-inline fixblock">
						<h4>Найденные ошибки:</h4>
						 <div class="editable" contenteditable="true">
							
							@if ($task->errors != '')
								{!! $task->errors !!}
							@else
								<ol>
									<li>Божечки, ничего не работает!</li>
								</ol>
							@endif
						</div>
						<br>
						<a href="{{ $blank }}" target="_blank"><img src="../../img/invoice.png" title="Бланк заказа" /> Бланк заказа</a> |
						<a href="http://{{$url}}" target="_blank">Перейти на сайт</a> | <a href="/view-pdf/{{$task->work_id}}" target="_blank">Просмотр отчета</a> | <a href="/history/{{$task->work_id}}" target="_blank">История изменений</a>
						<br><br>
							
							
							@if(Auth::user()->id == 61)
							<a  onclick="finishTest(0)" class="btn btn-warning" style="margin:auto;width:25%">
							   Закончить проверку
							</a>
							<a onclick="complete({{$task['work_id']}}, {{$task['status_id']}})" class="btn btn-primary" style="margin:auto;width:25%">
							   Сдать проект
							</a>
							@else
							<a  onclick="finishTest(0)" class="btn btn-warning" style="margin:auto;width:50%">
							   Закончить проверку
							</a>
							@endif
							<br>
							<a onclick="finishTest(3)" class="btn btn-success" style="margin:auto;width:25%;margin-top:3px;">
							   Отправить в готовые
							</a>
							<a onclick="errorReport()" class="btn btn-info" style="margin:auto;width:25%;margin-top:3px;">
							   Отправить ошибки миньону
							</a>
							<br><br>
							<style>
								img.task_icon2{
									height: 95px;
								    position: absolute;
								    top: 306px;
								    right: 225px;
								    border: 1px solid #b91621;
								    border-radius: 5px;
								    background-color: #b91621;
								}
							</style>
							<!--<a class="complete" style="cursor:pointer;" onclick="complete({{$task['work_id']}}, {{$task['status_id']}})" title="Сдать проект"><img class="task_icon2" src="../../img/lubo.png"/></a>-->

				</div>
				
				
				<div class="block">
						<h2 style="cursor:pointer" onclick="togMe(this)"><span class="caret"></span> Проверка серверной и административной части сайта</h2>
						<div style="text-align:left; width: 66%;" class="server-side">
							<ul>
								<li>Проверить актуальность списка дополнительных модулей:</li>
									<ul>
										<li>Проверить лишние модули в админ панели, за исключением:</li>
											<ul>
												<li>Файловый менеджер</li>
												<li>Регистрация пользователей</li>
												<li>Группы пользователей</li>
											</ul>
										<li>При наличии лишних модулей запросить у дежурного координатора информацию – почему они есть. Координатор должен предоставить бланк заказа оплаченного клиентом, в котором фигурирует данный модуль</li>
										<li>Проверить не достающие модули в админ панели</li>
										<li>При наличии недостающий модулей надо обратиться к координатору проекта, который должен предоставить дополнительное соглашение с клиентом, в котором написан отказ от данного модуля</li>
									</ul>
								<li>Выставить права на папки cache, data, imgtmp 0777 рекурсивно</li>
								<li>Если нет интеграции с 1С удалить из корня файлы 1c_cache.txt и 1c_exchange.php</li>
								<li>Если нет смс центра удалить из корня extraSystem.php и extraSystem.class.php</li>
								<li>Папку админки переименовать из «admin» в рандомный набор символов</li>
								<li>Задать значение параметров «Email для уведомлений о заказах» (если есть магазин),  «E-mail для восстановления пароля» и «Email» в Редактируемых полях в формате «admin@домен_сайта»</li>
								<li>При заливке на поддомен проверить наличие в корне сайта файла «robots.txt» содержащего одну строку «Disallow: *»</li>
								<li>При заливке на основной домен удалить «robots.txt» содержащий одну строку «Disallow: *»</li>
							</ul>
						</div>
				</div>
							<hr>
							<style type="text/css">
							  .dz-success-mark, .dz-error-mark {
							    display: none;
							  }
							}</style>
							<div id="comments">
							
 							<h2>Комментарии</h2>
							<input type="text" name="message" placeholder="Суть токова..." class="comm-message" style="height: 36px;" />
							<a onclick="sendComm()" class="btn btn-info" style="margin:auto;border-radius: 0px;margin-left: -4px;">
							   Отправить
							</a><br><br>
							
    						<form enctype="multipart/form-data" id="my-dropzone" role="form" method="POST" action="/network/upload" >
							    <!-- <h3 style="color:darkgrey;"><b>+</b><br>ПЕРЕТАЩИТЕ ФАЙЛ<br>ИЛИ НАЖМИТЕ НА ОБЛАСТЬ</h3> -->
							    <input type="hidden" name="_token" value="{{ csrf_token() }}">
							    <input type="hidden" name="work_id" value="{{$task['work_id']}}">

    						</form>
    						<img id="img-thumb" class="user size-lg img-thumbnail" src="">
							<hr>

							@foreach($comments as $comment)
							<div class="well well-sm comment">
								<p class="comment-date">{{$comment->created_at}}</p> <p class="comment-author">{{$comment->author}}</p>
								<div class="clear"></div>
								<div class="comment-body" style="display:block;">{{$comment->comment}}</div>
								@if($comment->attachments != "")
								<hr>
								<h4><u>Вложения:</u></h4>
								<div>
								<a href="{{$comment->attachments}}" data-lightbox="{{$comment->attachments}}">
								<img id="img-thumb" class="thumb" src="{{$comment->attachments}}">
								</a>
								
								</div>
								@endif
							</div>
							@endforeach

							</div>

        </div>

    </div>
				
</div>
@endsection

<script type="text/javascript" src="http://oracle.ru/js/jquery-1.9.1.js"></script>
<script type="text/javascript" src="/js/dropzone.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.9.0/js/lightbox-plus-jquery.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.9.0/css/lightbox.css">
<script>
    lightbox.option({
      'resizeDuration': 200,
      'wrapAround': true,
      'disableScrolling':	true,

    })
</script>


<script>
var filepath = '';


function togMe(cont){
	
	$(cont).next().toggle('slow');
}

	function complete(id, status){
		console.log(status);
		if(status != 2){
			bootbox.alert("В проекте остались ошибки, или проект ожидает перепроверки", function() {
            });
		}else if(status == 2){
			$.get(
			"/network/complete/",
				{
					'id':			id,
					'_token': 		'{!! csrf_token() !!}'
				},
			onAjaxSuccessComplete
			);
		}
			
	}
	function onAjaxSuccessComplete(data){
		bootbox.alert("Проект сдан!", function() {
				$("#tasks").load("/ #tasks");
            });
	}

function rateUp(){
	$('.label-success').fadeIn(2000);
	$('.label-success').fadeOut(2000);
}
function fieldIsNull(){
	$('.label-danger').fadeIn(2000);
	$('.label-danger').fadeOut(2000);
}

function errorReport(){
	errors = $('.editable').html();
			$.get(
			"/network/report-verstalshik/",
				{
					'id':			{{$task['work_id']}},
					'errors':		errors,
					'_token': 		'{!! csrf_token() !!}'
				},
			onAjaxReportSuccess
			);
}

function finishTest(isReport){
	
	console.log(isReport);
	errors = $('.editable').html();
		if (isReport == 0){
			$.get(
			"/network/finishTest/",
				{
					'id':			{{$task['work_id']}},
					'errors':		errors,
					'_token': 		'{!! csrf_token() !!}'
				},
			onAjaxSuccessFinish
			);
		}else if(isReport == 1){
			$.get(
			"/network/finishTest/",
				{
					'id':			{{$task['work_id']}},
					'errors':		errors,
					'_token': 		'{!! csrf_token() !!}'
				},
			onAjaxSuccessFinishViaReport
			);
		}else if(isReport == 3){
			$.get(
			"/network/ready/",
				{
					'id':			{{$task['work_id']}},
					'errors':		errors,
					'_token': 		'{!! csrf_token() !!}'
				},
			onAjaxSuccessReady
			);
		}
		
			
}

function sendComm(){
	comment = $('.comm-message').val();
	author = '{{Auth::user()->name}}';
	$.get(
			"/network/sendComm",
				{
					'id':			{{$task['work_id']}},
					'comment':		comment,
					'author': 		author,
					'filepath':     filepath,
					'_token': 		'{!! csrf_token() !!}'
				},
			onAjaxSuccessComment
			);
}



function onAjaxSuccessComment(data){
	if (data == "Success")
		$("#comments").load("http://checker.ru/edit/task/{{$task['work_id']}} #comments");
}


function onAjaxReportSuccess(data){
	bootbox.alert("Отчет об ошибках отправлен верстальщику.");
}

function onAjaxSuccessFinishViaReport(data)
			{
				window.location.href = "/create-pdf/{{$task['work_id']}}";
			}
function onAjaxSuccessFinish(data)
			{
				bootbox.alert("Задача проверена и ждет корректировок.");
			}
function onAjaxSuccessReady(data)
			{
				bootbox.alert("Статус задачи сменен на готова.", function() {
            });
			}



$(document).ready(function() {
  //Dropzone.js Options - Upload an image via AJAX.
  Dropzone.options.myDropzone = {
    uploadMultiple: false,
    // previewTemplate: '',
    addRemoveLinks: false,
    // maxFiles: 1,
    dictDefaultMessage: '',
    init: function() {
      this.on("addedfile", function(file) {
        // console.log('addedfile...');
      });
      this.on("thumbnail", function(file, dataUrl) {
        // console.log('thumbnail...');
        $('.dz-image-preview').hide();
        $('.dz-file-preview').hide();
      });
      this.on("success", function(file, res) {
      	filepath = res.path;
        console.log(filepath);
        $('#img-thumb').attr('src', res.path);
        $('input[name="pic_url"]').val(res.path);

      });
    }
  };
  var myDropzone = new Dropzone("#my-dropzone");
 
  $('#upload-submit').on('click', function(e) {
    e.preventDefault();
    //trigger file upload select
    $("#my-dropzone").trigger('click');
  });
 
});
 
//we want to manually init the dropzone.
Dropzone.autoDiscover = false;
$('#my-dropzone').unbind('click');



</script>

