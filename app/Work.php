<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Work extends Model{
    protected $table = 'works';

	public function work(){
	    return $this->belongsTo(Zeon\Work::class);
	}
}
