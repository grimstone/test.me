<?php

namespace App\Zeon;

use App\CitronModel as Model;

/**
 * Class Complect
 * @package App\Zeon
 */
class Complect extends Model{

	/**
	 * @var string
	 */
	protected $connection = 'zeon';
	/**
	 * @var string
	 */
	protected $table = 'ctrl_complects';

	/**
	 * Возвращает список допмодулей сайта
	 * @return mixed
	 */
	public  function modules(){
		return $this->belongsToMany('App\Zeon\Module', 'ctrl_complect_xref')->orderBy('sort');
	}

	/**
	 * Возвращает распечатаный список допмодулей сайта
	 * @return string
	 */
	public function print_modules( $div = true ){
		$str = '';
		if( $div ){
			foreach( $this->modules as $module )
				$str .= "<div>{$module['title']}</div>";
		}else{
			$str = [];
			foreach( $this->modules as $module )
				$str[] = $module['title'];
			$str = implode("\r\n", $str);
		}
		return $str;
	}
}
