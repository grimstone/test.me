<?php

namespace App\Zeon;

use App\CitronModel as Model;

class WorkFile extends Model{
	
	protected $connection = 'zeon';
	protected $table = 'zeon_work_files';
}
