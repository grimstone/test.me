<?php

namespace App\Zeon;

use App\CitronModel as Model;

class UserRight extends Model{

	protected $connection = 'zeon';
	protected $table = 'ctrl_user_rights';

	public function users(){
		return $this->belongsToMany('App\Zeon\User', 'ctrl_user_rights_xref', 'user_id', 'right_id');
	}
}
