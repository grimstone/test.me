<?php

namespace App\Zeon;

use App\CitronModel as Model;

class WorkHistory extends Model{

	protected $connection = 'zeon';
	protected $table = 'zeon_works_history';
	protected $dates = ['date'];

	public function themes(){
		return $this->belongsToMany('App\Zeon\WorkHistoryTheme', 'zeon_work_history_to_themes', 'history_id', 'theme_key');
	}
}
