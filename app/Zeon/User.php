<?php

namespace App\Zeon;

use App\CitronModel as Model;

class User extends Model{

	protected $connection = 'zeon';
	protected $table = 'ctrl_users';

	public function oracle_user(){
		return $this->hasOne('App\Oracle\User', 'system_id')->where('system', 'zeon');
	}

	public function rights(){
		return $this->belongsToMany('App\Zeon\UserRight', 'ctrl_user_rights_xref', 'user_id', 'right_id');
	}

	public function hasRight($right_id){
		return !$this->rights->filter(function($right) use ($right_id){
			return $right->id == $right_id;
		})->isEmpty();
	}

	public function getKvalif( $type = 'name' ){
		$kvalif = [
			[
				'name' => 'Специалист',
				'suff' => ''
			],
			[
				'name' => 'Ведущий',
				'suff' => '_vidu'
			],
			[
				'name' => 'Эксперт',
				'suff' => '_eksp'
			],
		];
		if( $type == 'name' )
			return ' (Квалификация '.$kvalif[ $this->kvalif ][$type].') ';
		else
			return $kvalif[ $this->kvalif ][$type];
	}
}
