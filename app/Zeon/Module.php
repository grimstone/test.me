<?php

namespace App\Zeon;

use App\CitronModel as Model;

class Module extends Model{

	protected $connection = 'zeon';
	protected $table = 'ctrl_work_modules';
}
