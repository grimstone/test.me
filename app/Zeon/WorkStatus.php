<?php

namespace App\Zeon;

use App\CitronModel as Model;

class WorkStatus extends Model{

	protected $connection = 'zeon';
	protected $table = 'zeon_work_status';

	public function works(){
		return $this->belongsToMany('App\Zeon\Work', 'status');
	}
}
