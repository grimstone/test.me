<?php

namespace App\Zeon;

use App\CitronModel as Model;

/**
 * Class Work
 * @package App\Zeon
 */
class Work extends Model{
	protected $dates = ['date_add', 'date_next', 'date_end', 'diz_date_add', 'diz_date_next'];

	/**
	 * @var string
	 */
	protected $connection = 'zeon';
	/**
	 * @var string
	 */
	protected $table = 'zeon_works';

	public function oracle_user(){
		return $this->hasOne('App\Oracle\User', 'system_id');
	}
	/**
	 * Возвращает история задачи
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function histories(){
		return $this->hasMany('App\Zeon\WorkHistory');
	}

	/**
	 * Возвращает анкетные данные задачи
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function anketa(){
		return $this->hasOne('App\Zeon\WorkAnketa');
	}

	/**
	 * Возвращает файлы задачи
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function files(){
		return $this->hasMany('App\Zeon\WorkFile');
	}

	/**
	 * Возвращает файлы дизайна задачи
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function diz_files(){
		return $this->hasMany('App\Zeon\WorkFileDiz');
	}

	/**
	 * Возвращает файлы корректировок дизайна задачи
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function diz_cor_files(){
		return $this->hasMany('App\Zeon\WorkFileDizCor');
	}

	/**
	 * Возвращает бланк задачи
	 * @return \Illuminate\Database\Eloquent\Relations\HasOne
	 */
	public function blank(){
		return $this->hasOne('App\Oracle\Blank', 'id', 'base_id');
	}

	/**
	 * Коорданатор
	 * @return \Illuminate\Database\Eloquent\Relations\HasOne
	 */
	public function coordinator(){
		return $this->hasOne('App\Zeon\User', 'id', 'coord');
	}

	/**
	 * Дизайнер
	 * @return \Illuminate\Database\Eloquent\Relations\HasOne
	 */
	public function designer(){
		return $this->hasOne('App\Zeon\User', 'id', 'diz');
	}

	/**
	 * Верстальщик
	 * @return \Illuminate\Database\Eloquent\Relations\HasOne
	 */
	public function programmist(){
		return $this->hasOne('App\Zeon\User', 'id', 'prog');
	}

	/**
	 * Статус
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function istatus(){
		return $this->belongsTo(WorkStatus::class, 'status');
	}

	/**
	 * Возвращает название статуса задачи
	 * @return mixed
	 */
	public function get_status_name(){
		return \Cache::remember('status.'.$this->status, 10, function(){
			$status = \App\Zeon\WorkStatus::find($this->status);
			if( is_null($status) )
				$name = '';
			else
				$name = $status->name;
			return $name;
		});
	}

	/**
	 * Возвращает имя продажника задачи
	 * @return mixed
	 */
	public function get_seller_name(){
		return \Cache::remember('oracle.user.'.$this->blank->user_id, 10 , function(){
			return \App\Oracle\User::find($this->blank->user_id)->name;
		});
	}

	/**
	 * Возвращает имя координатора задачи
	 * @return string
	 */
	public function get_coord_name(){
		return \Cache::remember('zeon.user.'.$this->coord, 10, function(){
			$user = \App\Zeon\User::find($this->coord);
			if( is_null($user) )
				$name = '';
			else
				$name = $user->name;
			return $name;
		});
	}

	/**
	 * Возвращает имя дизайнера, что делал задачу
	 * @return string
	 */
	public function get_diz_name(){
		return \Cache::remember('zeon.user.'.$this->diz, 10, function(){
			$user = \App\Zeon\User::find($this->diz);
			if( is_null($user) )
				$name = '';
			else
				$name = $user->name;
			return $name;
		});

	}

	/**
	 * Возвращает имя верстальщика\программиста, что делал задачу
	 * @return string
	 */
	public function get_prog_name(){
		return \Cache::remember('zeon.user.'.$this->prog, 10, function(){
			$user = \App\Zeon\User::find($this->prog);
			if( is_null($user) )
				$name = '';
			else
				$name = $user->name;
			return $name;
		});
	}

	/**
	 * Координатор общения
	 * @return mixed
	 */
	public function get_blablacoord_name(){
		return \Cache::remember('zeon.user.'.$this->prog, 10, function(){
			$user = \App\Zeon\User::find($this->blank->blablacoord);
			if( is_null($user) )
				$name = '';
			else
				$name = $user->name;
			return $name;
		});
	}

	/**
	 * Возвращает название типа задачи
	 * @return string
	 */
	public function get_type_name(){
		return $this->type == 1 ? 'Под ключ' : 'Доработка';
	}

	/**
	 * Возвращает списка допмодулей бланка
	 * @return string
	 */
	public function print_modules( $div = true ){
		return \Cache::remember('print.blank.modules'.$this->id, 10, function() use ($div){
			$str = '';
			if( $div ){
				foreach( $this->blank->modules as $module )
					$str .= "<div>{$module->title}</div>";
			}else{
				$str = [];
				foreach( $this->blank->modules as $module )
					$str[] = $module->title;
				$str = implode("\r\n", $str);
			}
			return $str;
		});
	}

	/**
	 * Возвращает списка всех модулей бланка
	 * @return string
	 */
	public function print_all_modules( $div = true ){
		$true = $div ? 'true' : 'false';
		return \Cache::remember("print.all.modules.{$true}.".$this->id, 10, function() use ($div){
			$str = '';
			if( $div ){
				if( !is_null( $this->blank->icomplect ) )
					$str .= $this->blank->icomplect->print_modules( $div );

				if( !is_null( $this->blank->ishop ) )
					$str .= $this->blank->ishop->print_modules( $div );

				$str .= $this->print_modules( $div );
			}else{
				$str = [];

				if( !is_null( $this->blank->icomplect ) )
					$str[] = $this->blank->icomplect->print_modules( $div );

				if( !is_null( $this->blank->ishop ) )
					$str[] = $this->blank->ishop->print_modules( $div );

				$str[] = $this->print_modules( $div );
				$str = implode("\r\n", $str);
			}
			return $str;
		});
	}

	/**
	 * Рассчитывает премию за работу для роли
	 * @param string $type
	 * @return \App\Zeon\Prem
	 */
	public function get_prem( $type ){
		return new Prem($type, $this);
		return \Cache::remember("prem.{$type}.{$this->id}", 10, function() use ($type){
			return new Prem($type, $this);
		});
	}

	/**
	 * Возвращает процент оплаты
	 * @return int
	 */
	public function get_percent_payment(){
		return $this->blank->sum == $this->blank->contract_sum ? 100 : 50;
	}

	/**
	 * Выводит все модули проданные координатором
	 * @return mixed
	 */
	public function print_coord_modules() {
		return \Cache::remember('print.coord.modules.array.'.$this->blank->id, 10, function(){
			$str = '';
			$sum = 0;
			foreach( $this->blank->modules as $module ){
				if( $module->pivot->coord ){
					if( $module->pivot->count > 1 )
						$str .= "<div>{$module->title} ({$module->pivot->price} x {$module->pivot->count}шт)<div>";
					else
						$str .= "<div>{$module->title} ({$module->pivot->price})<div>";
					$sum += $module->pivot->count * $module->pivot->price;
				}
			}
			return [$str, $sum];
		});
	}

	/**
	 * Выводит все дизайнерские файлы работы
	 * @return mixed
	 */
	public function print_diz_files(){
		return \Cache::remember('print.diz.files'.$this->id, 5 , function(){
			$str = '';
			$files = $this->diz_files->filter(function($file){
				return $file->cor_num == $this->use_cor;
			});
			foreach( $files as $file ){
				$str .= '<div>';
				if( strpos($file->file, '.psd') !== false ){
					$str .= '<a href="http://zeon.ru/index/index/get_preview/id/'.$this->id.'/file/'.str_replace('.psd', '', $file->file).'.jpg" target="_blank" title="JPG"><img src="http://zeon.ru/images/eye.png" alt="JPG" style="cursor:pointer;"></a>';
				}
				$str .= '<a style="text-decoration:none;" href="http://zeon.ru/work/diz/'.$this->id.'/'.$file->file.'" target="_blank">'.$file->name.'</a>';
				$str .= "</div>";
			}
			return $str;
		});
	}

	/**
	 * Выводит все файлы работы
	 * @return mixed
	 */
	public function print_all_files(){
		return \Cache::remember('print.all.files'.$this->id, 10, function(){
			$str = '';

			foreach( $this->blank->files as $file )
				$str .= '<div><a href="http://oracle.ru/blanks/files/'.$file->file.'" target="_blank">'.$file->name.'</a></div><hr>';

			foreach( $this->diz_cor_files as $file )
				$str .= '<div><a href="http://zeon.ru/work/cor/'.$this->id.'/'.$file->file.'" target="_blank">Заявка на коррекцию №'.$file->diz_true.'</a></div><hr>';

			foreach( $this->files as $file )
				$str .= '<div><a href="http://zeon.ru/work/files/'.$this->id.'/'.$file->file.'" target="_blank">'.$file->name.'</a></div><hr>';

			$str .= '<div><a href="http://oracle.ru/blanks/docs/'.$this->base_id.'.html" target="_blank"> Бланк Заказа </a></div><hr>';

			return $str;
		});

	}

	/**
	 * Проверяет подтверждена ли верстка
	 * @return string
	 */
	public function vestka_is_checked(){
		return $this->verstka_cheked ? 'Да' : 'Нет';
	}

	/**
	 * Возвращает цвет работы
	 * @return string
	 */
	public function get_color(){
		$str = '';
		if( in_array($this->status, array(1, 13, 14, 16, 19, 18) ) )
			$str = 'red';
		elseif($this->status == 3 || $this->status == 6 || $this->status == 15 )
			$str = 'yellow';
		elseif($this->status == 9 )
			$str = 'brown';
		elseif($this->status == 10)
			$str = 'blue';
		elseif($this->status == 11)
			$str = 'fio';
		return $str;
	}

	/**
	 * Проверка доступа
	 * @param $user
	 * @param string $type
	 * @return bool
	 */
	public function hasAccess(&$user, $type = 'coord'){

		if( $user->group == 1 )
			return true;

		if( $type == 'coord' && $user->hasRight(1) && $this->coord == $user->id )
			return true;

		if( $type == 'prog' && $user->hasRight(2) && $this->prog == $user->id )
			return true;

		if( $type == 'diz' && $user->hasRight(4) && $this->diz == $user->id )
			return true;

		return false;
	}

	/**
	 * Проверка на право обладать кнопкой редактирования
	 * @param $user
	 * @param string $type
	 * @return string
	 */
	public function showControlButton(&$user, $type = 'coord'){
		if( $this->hasAccess($user, $type) ){
			if( $type == 'coord' )
				return ' class="button" onclick="showWorkVariants('.$this->id.');"';
		}
		return '';
	}

	/**
	 * Выводит все договора
	 * @return mixed
	 */
	public function print_contacts(){
		return \Cache::remember('print.contacts'.$this->id, 10, function(){
			$str = '';
			foreach( $this->blank->get_client()->contacts as $contact )
				foreach( ['name', 'phone', 'email'] as $val )
					if( !empty($contact->$val) )
						$str .= "<div>{$contact->$val}</div><br>";
			return $str;
		});

	}

	/**
	 * Санкции
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function penaltys(){
		return $this->hasMany(Penalty::class);
	}

	/**
	 * Санкции по типу
	 * @param $type
	 * @return mixed
	 */
	public function get_penaltys($type){
		return $this->penaltys()->where('role', $type)->get();
	}
}
