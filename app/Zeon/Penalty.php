<?php

namespace App\Zeon;

use App\CitronModel as Model;

class Penalty extends Model{

	protected $connection = 'zeon';
	protected $table = 'zeon_penaltys';
	protected $dates = ['date'];

	public function work(){
		return $this->belongsTo(Work::class);
	}
}
