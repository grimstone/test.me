<?php

namespace App\Zeon;

use App\CitronModel as Model;

class WorkHistoryTheme extends Model{

	protected $connection = 'zeon';
	protected $table = 'zeon_work_history_themes';
	//protected $primaryKey = 'key';
}
