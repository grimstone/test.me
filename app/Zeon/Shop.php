<?php

namespace App\Zeon;

use App\CitronModel as Model;

/**
 * Class Shop
 * @package App\Zeon
 */
class Shop extends Model{

	/**
	 * @var string
	 */
	protected $connection = 'zeon';
	/**
	 * @var string
	 */
	protected $table = 'ctrl_shops';

	/**
	 * Возвращает список допмодулей магазина
	 * @return mixed
	 */
	public  function modules(){
		return $this->belongsToMany('App\Zeon\Module', 'ctrl_shop_xref')->orderBy('sort');
	}

	/**
	 * Возвращает распечатаный список допмодулей магазина
	 * @return string
	 */
	public function print_modules( $div = true ){
		$str = '';
		if( $div ){
			foreach( $this->modules as $module )
				$str .= "<div>{$module['title']}</div>";
		}else{
			$str = [];
			foreach( $this->modules as $module )
				$str[] = $module['title'];
			$str = implode("\r\n", $str);
		}
		return $str;
	}
}
