<?php

namespace App\Zeon;


/**
 * Class Prem
 * @package App\Zeon
 */
class Prem{

	/**
	 * @var array
	 */
	public $distanation = [];
	/**
	 * @var int
	 */
	public $sum = 0;
	/**
	 * @var int
	 */
	public $sum_clear = 0;

	/**
	 * @var array
	 */
	private $diz_porog = [
		'min' => 50,
		'max' => 400
	];
	/**
	 * @var array
	 */
	private $prog_porog = [
		'min' => 50,
		'max' => 400
	];
	/**
	 * @var array
	 */
	private $coord_porog = [
		'min' => 300
	];

	/**
	 * Список процентов премий
	 * @var array
	 */
	private $percent = [
		'diz_module'    => 0.05,
		'diz_logo'      => 0.1,
		'prog_module'   => 0.05,
		'prog_dorab'    => 0.05,
		'coord'         => 0.05
	];

	/**
	 * Prem constructor.
	 * @param string $type
	 * @param \App\Zeon\Work $work
	 * @throws \Exception
	 */
	public function __construct( $type, &$work){

		if( $type == 'coord' ){
			$percent = $this->percent['coord'] * 100;
			$this->sum = $work->blank->contract_sum * $this->percent['coord'];

			if( !$work->coordinator->hasRight(5) ){
				if( $this->sum < $this->coord_porog['min'] )
					$this->sum = $this->coord_porog['min'];

				$this->distanation[] = sprintf( "%s от суммы договора %d руб при минимальной премии в %d руб = %d руб", $percent."%", $work->blank->contract_sum, $this->coord_porog['min'], $this->sum );
			}else{
				$this->distanation[] = sprintf( "%s от суммы договора %d руб", $percent."%", $work->blank->contract_sum );
			}
			$this->sum_clear = $this->sum;
			if( !is_null( $work->anketa ) ){
				if( $work->anketa->rating_coord == 3 ){
					$this->distanation[] = '-50% за оценку «3»';
					$this->sum = $this->sum / 2;
				}else if( $work->anketa->rating_coord == 2 ){
					$this->distanation[] = '-100% за оценку «2»';
					$this->sum = 0;
				}
			}
			$this->get_penalty($work, 'coord');
		}else if( $type == 'prog' ){
			if( $work->blank->is_shop() && is_null($work->blank->ishop) ){
				$prem = Complect::find(0);
				$prem = $prem->{"prog_".$work->blank->elit_eng_name().$work->programmist->getKvalif('suff')};
				$this->sum = $prem;
				$this->distanation[] = $work->blank->elit_name().' Магазин'.$work->programmist->getKvalif().' ('.$prem.')';
			}else if( $work->blank->is_shop() ){
				$prem = $work->blank->ishop->{"prog_".$work->blank->elit_eng_name().$work->programmist->getKvalif('suff')};
				$this->sum = $prem;
				$this->distanation[] = $work->blank->elit_name().' магазин '.$work->blank->ishop->name.$work->programmist->getKvalif()." ({$prem})";
			}else if( $work->blank->complect ){
				$prem = $work->blank->icomplect->{"prog_".$work->blank->elit_eng_name().$work->programmist->getKvalif('suff')};
				$this->sum = $prem;
				$this->distanation[] = $work->blank->elit_name().' магазин '.$work->blank->icomplect->name.$work->programmist->getKvalif()." ({$prem})";
			}else if( $work->type != 1 ){
				$prem = $work->blank->contract_sum * $this->percent['prog_dorab'];

				$prem = $prem < $this->prog_porog['min'] ? $this->prog_porog['min'] : $prem;
				$prem = $prem > $this->prog_porog['max'] ? $this->prog_porog['max'] : $prem;

				$this->sum = $prem;
				$percent = $this->percent['prog_dorab'] * 100;
				$this->distanation[] = sprintf("Доработка: %s от стоимости в %d руб, в пределах %d-%d руб = %d руб ",
					$percent."%",
					$work->blank->contract_sum,
					$this->prog_porog['min'],
					$this->prog_porog['max'],
					$prem
				);
			}
			if( $work->type == 1 )
				foreach( $work->blank->modules as $module ){
					$name = mb_strtolower($module->name);
					if( mb_strpos( $name, 'хостинг' ) === false && mb_strpos( $name, 'верстк' ) !== false ){
						$prem = $module->pivot->price * $module->pivot->count * $this->percent['prog_module'];

						$prem = $prem < $this->prog_porog['min'] ? $this->prog_porog['min'] : $prem;
						$prem = $prem > $this->prog_porog['max'] ? $this->prog_porog['max'] : $prem;

						$this->sum += $prem;
						$this->distanation[] = $module['title']." ({$prem})";
					}
				}

			$this->sum_clear = $this->sum;
			if( !is_null($work->anketa) ){
				if( $work->anketa->rating_prog == 3 ){
					$this->distanation[] = '-50% за оценку «3»';
					$this->sum = $this->sum / 2;
				}else if( $work->anketa->rating_prog == 2 ){
					$this->distanation[] = '-100% за оценку «2»';
					$this->sum = 0;
				}
			}

			$this->get_penalty($work, 'prog');
			if( $work->prog_300 != 0 ){
				if( $work->prog_300 == -300 )
					$this->distanation[] = "-300 за не выполнение месячной нормы";
				else
					$this->distanation[] = "+300 за перевыполнение месячной нормы";

				$this->sum += $work->prog_300;
			}
		}else if( $type == 'diz' ){
			if( $work->blank->is_shop() && is_null($work->blank->ishop) ){
				$prem = Complect::find(0);
				$prem = $prem->{"prem_".$work->blank->elit_eng_name().$work->designer->getKvalif('suff')};
				$this->sum = $prem;
				$this->distanation[] = $work->blank->elit_name().' Магазин'.$work->designer->getKvalif().' ('.$prem.')';
			}else if( $work->blank->is_shop() ){
				$prem = $work->blank->ishop->{"prem_".$work->blank->elit_eng_name().$work->designer->getKvalif('suff')};
				$this->sum = $prem;
				$this->distanation[] = $work->blank->elit_name().' магазин '.$work->blank->ishop->name.$work->designer->getKvalif()." ({$prem})";
			}else if( $work->blank->complect ){
				$prem = $work->blank->icomplect->{"prem_".$work->blank->elit_eng_name().$work->designer->getKvalif('suff')};
				$this->sum = $prem;
				$this->distanation[] = $work->blank->elit_name().' магазин '.$work->blank->icomplect->name.$work->designer->getKvalif()." ({$prem})";
			}

			foreach( $work->blank->modules as $module ){
				if( $module->pivot->diz_prem ){
					$prem = $module->pivot->price * $this->percent['diz_module'];
					if( strpos($module['name'], 'логотип') === false ){
						$prem = $prem < $this->diz_porog['min'] ? $this->diz_porog['min'] : $prem;
						$prem = $prem > $this->diz_porog['max'] ? $this->diz_porog['max'] : $prem;
					}else{
						$prem = $module->pivot->price * $this->percent['diz_logo'];
					}
					$this->sum += $prem * $module->pivot->count;
					if( $module->pivot->count > 1 )
						$this->distanation[] = $module['title']." ({$prem} x {$module->pivot->count}шт)";
					else
						$this->distanation[] = $module['title']." ({$prem})";
				}
			}

			$this->sum_clear = $this->sum;
			if( !is_null($work->anketa) ){
				if( $work->anketa->rating_diz == 3 ){
					$this->distanation[] = '-50% за оценку «3»';
					$this->sum = $this->sum / 2;
				}else if( $work->anketa->rating_diz == 2 ){
					$this->distanation[] = '-100% за оценку «2»';
					$this->sum = 0;
				}
			}

			$this->get_penalty($work, 'diz');
			if( $work->diz_300 != 0 ){
				if( $work->diz_300 == -300 )
					$this->distanation[] = "-300 за не выполнение месячной нормы";
				else
					$this->distanation[] = "+300 за перевыполнение месячной нормы";

				$this->sum += $work->diz_300;
			}
		}else
			throw new \Exception('Э! А где тип то премии?!');
	}

	private function get_penalty($work, $type){
		$penaltys = $work->get_penaltys($type);
		if( !$penaltys->isEmpty() ){
			foreach( $penaltys as $penalty ){
				$this->distanation[] = "-{$penalty->sum} санкция по причине «{$penalty->descr}»";
				$this->sum -= $penalty->sum;
			}
		}
	}
}