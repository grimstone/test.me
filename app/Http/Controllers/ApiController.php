<?php

namespace App\Http\Controllers;

use App\Zeon\Work;
use Illuminate\Http\Request;
use App\Task;
use Auth;
use App\Oracle\User as User;
use Gate;
use DB;

class ApiController extends Controller{

	public function import($id){
		$work = Work::find($id);
		if( is_null($work) )
			dd('Ты втираешь мне дичь!', $id);

		$old_work = \App\Work::where('work_id', $id)->first();
		if( !is_null($old_work) )
			dd('Это нечто уже поступило на проверку!');

		$new_work = new \App\Work();
		$new_work->work()->associate($work);
		$new_work->save();

		$sitename = $work->blank->contract->domen->name;
		$addTaskReport = new TasksController;
		$addTaskReport->addReport($id, 'Задача добавлена на тестирование.');
		$addTaskReport->slackMessage(200, 'Задача "'.$sitename.'" поступила на тестирование!');
		

		return redirect('/');
	}
}
