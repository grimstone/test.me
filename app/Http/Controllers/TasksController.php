<?php
namespace App\Http\Controllers;

use Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\HomeController;
use App\Work;
use App\Report;
//use App\User;
use Auth;
use PDF;
use Gate;
use App\Zeon\Work as ZeonWork;
use App\Oracle\User as User;
use Maknz\Slack\Client as Client;
use Storage;
use DB;

class TasksController extends Controller
{
	// /*Добавляем сайт*/
	// public function push()
	// {
	// 	$name 			= Request::input('name');
	// 	$type 			= Request::input('type');
	// 	$domen 			= Request::input('domen');
	// 	$poddomen	 	= Request::input('poddomen');
	// 	$blank	 		= Request::input('blank');
	// 	$price	 		= Request::input('price');
		
			
		
	// 	$task = new Task();
	// 		$task->title 	= $name;
	// 		$task->type 	= $type;
	// 		$task->domen 	= $domen;
	// 		$task->poddomen = $poddomen;
	// 		$task->blank 	= $blank;
	// 		if($price != '' || isset($price))
	// 			$task->price 	= $price;
	// 		$task->author 	= Auth::user()->name;
	// 	$task->save();
		
	// 	$report = new Report();
	// 		$report->site_id 	= $task->id;
	// 		$report->action 	= 'Задача добавлена.';
	// 		$report->author 	= Auth::user()->name;
	// 	$report->save();
		
	// 	return 'OK!';
	// }
	



	/*Удаляем сайт*/
	public function delete($id)
	{
		//$id = Request::input('name');
		
		Work::where('work_id', '=', $id)
			->delete();
		
		return 'deleted';
	}
	



	/*Редактирование сайта*/
	public function edit($id)
	{
		

		$homeController = new HomeController;
		$site = $homeController->getVerstka_check($id);
		$siteName = $site['site'];
		$blank = $site['blank'];
		if(strlen($site['poddomen']) < 3)
			$domen = $site['domen'];
		else
			$domen = $site['poddomen'];
		//$domen = $site['poddomen'] != '/' ? $site['poddomen'] : $site['domen'];
		$comments = DB::table('comments')->orderBy('created_at', 'DESC')->where('work_id', $id)
			->get();
		$user = ZeonWork::find($id)->coordinator->name;
		$isVerst = 0;
		if (Auth::user()->is_verstalshik())
			$isVerst = 1;

		if (!Auth::user()->is_checker()) {
			$task = Work::select('work_id', 'errors', 'status_id')->where('work_id', '=', $id)->get();
			return view('view-task', ['sitename' => $siteName,'task' => $task[0], 'user' => $user, 'blank' => $blank, 'url' => $domen, 'isVerst' => $isVerst, 'comments'=>$comments]);
			
        }
		$task = Work::select('work_id', 'errors', 'status_id')->where('work_id', '=', $id)->get();
		
					
		return view('task', ['sitename' => $siteName, 'task' => $task[0], 'user' => $user, 'blank' => $blank, 'url' => $domen, 'comments'=>$comments]);
	}
	


	
	public function finishTest(){
		$id 			= Request::input('id');
		$errors 		= Request::input('errors');
		
		
		
		
		Work::where('work_id', $id)
            ->update(array('errors' => $errors, 'status_id' => '1'));
        $this->addReport($id, 'Статус проекта сменился на "Проверен, обнаружены ошибки"', $errors);

        
        $coordinator_id = ZeonWork::find($id)->coordinator->oracle_user->id;
        $sitename = ZeonWork::find($id)->blank->contract->domen->name;
		$this->slackMessage($coordinator_id, "Тестирование задачи '".$sitename."' завершено, обнаружены ошибки.");
		return "ok";
	}
	



	public function retest(){
		$id 			= Request::input('id');
		
		Work::where('work_id', $id)
            ->update(array('status_id' => '3'));
        $this->addReport($id, 'Статус проекта сменился на "Ждет перепроверки"');
        $sitename = ZeonWork::find($id)->blank->contract->domen->name;
		$this->slackMessage(200, "Задача  '".$sitename."'(http://checker.ru/edit/task/".$id.") поступила на перепроверку");
		return "ok";
	}
	



	public function ready(){
		$id 			= Request::input('id');
		$errors 		= 'Все найденные ошибки устранены. Претензий к качеству нет.';
		
		Work::where('work_id', $id)
            ->update(array('status_id' => '2', 'errors' => $errors));
        $this->addReport($id, 'Статус проекта сменился на "Готов"');
        $coordinator_id = ZeonWork::find($id)->coordinator->oracle_user->id;
        $sitename = ZeonWork::find($id)->blank->contract->domen->name;
		$this->slackMessage($coordinator_id, "Задача '".$sitename."' проверена, ошибок не найдено.");
		if(ZeonWork::find($id)->type == 1)
			$this->slackMessage(61, "Задача '".$sitename."'(http://checker.ru/edit/task/".$id.") проверена, требуется перепроверка.");
		
		return "ok";
	}


	public function complete(){
		$id 			= Request::input('id');
		
		Work::where('work_id', $id)
            ->update(array('status_id' => '4'));
        $this->addReport($id, 'Проект сдан');
        $coordinator_id = ZeonWork::find($id)->coordinator->oracle_user->id;
        $sitename = ZeonWork::find($id)->blank->contract->domen->name; 
		$this->slackMessage($coordinator_id, "Поздравляем, проект '".$sitename."' сдан!.");
		
		return "ok";
	}
	
	


	/*Экспорт отчета в PDF*/
	public function pdf($id, $errors = null)
	{
		
		$sitename = Work::select('work_id', 'errors', 'created_at')->where('work_id', '=', $id)->first();
		
		
		$errors = '';
		if(isset($sitename->errors)){
			$errors = $sitename['errors'];
		}
		
		$homeController = new HomeController;
		$site = $homeController->getVerstka_check($id);
		$coordinator = ZeonWork::find($id)->coordinator->name;


		$data['site'] = $site['site'];
		$data['domen'] = $site['domen'];
		$data['type'] = $site['type'];
		$data['complect'] = $site['complect'];
		$data['blank'] = $site['blank'];
		$data['created_at'] = $sitename['created_at'];
		//$data['created_at'] = new DateTime($data['created_at']);
		//$data['created_at'] = $data['created_at'];
		$data['errors'] = $errors;
		$data['coordinator'] = $coordinator;

		
		return PDF::loadHTML(view('report', ['data' => $data])->render())->download('Отчет по проверке сайта '.$site['site'].'.pdf');
	}
	
	
	public function viewPDF($id){
		$sitename = Work::select('work_id', 'errors', 'created_at')->where('work_id', '=', $id)->first();
		
		
		$errors = '';
		if(isset($sitename->errors)){
			$errors = $sitename['errors'];
		}
		
		$homeController = new HomeController;
		$site = $homeController->getVerstka_check($id);
		$coordinator = ZeonWork::find($id)->coordinator->name;


		$data['site'] = $site['site'];
		$data['domen'] = $site['domen'];
		$data['type'] = $site['type'];
		$data['complect'] = $site['complect'];
		$data['blank'] = $site['blank'];
		$data['created_at'] = $sitename['created_at'];
		$data['errors'] = $errors;
		$data['coordinator'] = $coordinator;

		return view('report', ['data' => $data]);
	}


	/*История правок сайта*/
	public function history($id){
		$reports = Report::where('work_id', '=', $id)
		    ->orderBy('created_at', 'DESC')
			->get();
		
		return view('history', ['reports' => $reports]);
	}
	
	/*Реализация поиска*/
	public function search(){
		$query = Request::input('query');
		$homeController = new HomeController;
		// 

		$sites = ZeonWork::where('coord', 'LIKE', Auth::user()->zeon_user->id)->get();
		$searchResults = [];
		foreach ($sites as $site){
			if (isset($site->blank->contract->domen->name)){
				// print ($site->blank->contract->domen->name."\n");
				 $pos = strripos($site->blank->contract->domen->name, $query);
				 // echo $pos;
				if ($pos === FALSE){

				}
				else{
					array_push($searchResults, $homeController->getVerstka_check($site->id));

				}
			}
		}

		return $searchResults;
	}

/*Реализация поиска*/
	public function search2($query = 'blank'){
		//die(dd($query));
		$homeController = new HomeController;
		// 
		if(Auth::user()->is_checker()){
			$works = Work::get();
			$userWorks = [];
			$searchResults = [];
			$i = 0;
            foreach ($works as $work){
                $workParams = $homeController->getVerstka_check($work->work_id);
                if (stristr($workParams['site'], $query)<>FALSE){
	                $userWorks['id'] = $work->work_id;
	                $userWorks['site'] = $workParams['site'];
	                $userWorks['blank'] = $workParams['blank'];
	                $userWorks['domen'] = $workParams['domen'];
	                $userWorks['poddomen'] = $workParams['poddomen'];
	                $userWorks['type'] = $workParams['type'];
	                $userWorks['verst'] = $workParams['verst'];
	                $userWorks['complect'] = $workParams['complect'];
	                $userWorks['status'] = $work->status_id;
	                $userWorks['author'] = ZeonWork::find($work->work_id)->coordinator->name;
	                //$work->userWorks = $userWorks;
	                array_push($searchResults, $userWorks);
	                //echo($work->userWorks['name']);
	                $i++;
                }
            }
            //die(dd($searchResults));
			return view('search', ['searchResults' => $searchResults]);
		}else if(Auth::user()->is_coordinator()){
			$sites = ZeonWork::where('coord', '=', (int)Auth::user()->zeon_user->id)
			->orWhere('coord', 'LIKE', (int)Auth::user()->zeon_user->id)
			->get();
		
			$searchResults = [];
			foreach ($sites as $site){
				if (isset($site->blank->contract->domen->name)){
					 
					 $pos = (strstr($site->blank->contract->domen->name, $query));
					 // echo $pos;
					if ($pos <> FALSE){
						$status = Work::where('work_id', $site->id)->select('status_id')->first();
						$status_work = $status['status_id'];
						if(!is_null($status_work)){
							$task = $homeController->getVerstka_check($site->id);
							$task['status'] = $status_work;
							array_push($searchResults, $task);
						}
						//$searchResults[] = $status['status_id'];
						//die(dd($searchResults));
					}
				}
			}
			return view('search', ['searchResults' => $searchResults]);
		}else if(Auth::user()->is_verstalshik()){
            $sites = \App\Zeon\Work::where('prog', 'LIKE', (int)Auth::user()->zeon_user->id)
                ->orWhere('prog', 'LIKE', (int)Auth::user()->zeon_user->id)
                ->get();

            $searchResults = [];
			foreach ($sites as $site){
				if (isset($site->blank->contract->domen->name)){
					 
					 $pos = (strstr($site->blank->contract->domen->name, $query));
					 // echo $pos;
					if ($pos <> FALSE){
						$status = Work::where('work_id', $site->id)->select('status_id')->first();
						$status_work = $status['status_id'];
						if(!is_null($status_work)){
							$task = $homeController->getVerstka_check($site->id);
							$task['status'] = $status_work;
							array_push($searchResults, $task);
						}
						//$searchResults[] = $status['status_id'];
						//die(dd($searchResults));
					}
				}
			}
			return view('search', ['searchResults' => $searchResults]);
		}
	}


	/*Запись в историю*/
	public function addReport($id, $action, $errors = null){
		//Report::insert(array('work_id' => $id, 'action' => $action, 'author' => Auth::user()->name));
		$report = new Report();
	 		$report->work_id 	= $id;
	 		$report->action 	= $action;
	 		$report->author 	= Auth::user()->name;
	 		$report->errors 	= $errors;
	 	$report->save();

	}



	/*КОД ДЛЯ ОТПРАВКИ СООБЩЕНИЯ*/
	public function slackMessage($id, $text){
		$settings = [
		    'username' => 'Жуколов',
		    'channel' => '#accounting'
		];
		$client = new Client('https://hooks.slack.com/services/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', $settings);
		$slack_name = User::find($id)->slack_name;

		$client->to('@'.$slack_name)->withIcon(':bug:')->send($text);
	}


	public function vseSdelanoGospodin(){
		$id 			= Request::input('id');

		$sitename = ZeonWork::find($id)->blank->contract->domen->name;

		$text = 'В задаче "'.$sitename.'" поправлены найденные ошибки (см. здесь http://checker.ru/edit/task/'.$id.').';
		
		$coordinator_id = ZeonWork::find($id)->coordinator->oracle_user->id;

		Work::where('work_id', $id)
            ->update(array('status_id' => '3'));
        
        $this->addReport($id, 'Статус проекта сменился на "Ждет перепроверки"');
		$this->slackMessage(200, "Задача  '".$sitename."'(http://checker.ru/edit/task/".$id.") поступила на перепроверку");

		$this->slackMessage($coordinator_id, $text);
	}


	public function sendComm(){
		$id 			= Request::input('id');
		$author 		= Request::input('author');
		$comment 		= Request::input('comment');
		$filepath 		= Request::input('filepath');

		DB::table('comments')->insert(
  			[
  			'work_id'		=> 	$id, 
  			'comment' 		=> 	$comment, 
  			'author' 		=> 	$author,
  			'attachments' 	=> 	$filepath
  			]
		);
		return "Success";	


	}

	public function upload() {


    if(Request::hasFile('file')) {
      


      $file = Request::file('file');
      $work_id = Request::input('work_id');
      $tmpFilePath = "/img/".$work_id."/";
      $tmpFileName = time() . '-' . $file->getClientOriginalName();
      $file = $file->move(public_path() . $tmpFilePath, $tmpFileName);
      $path = $tmpFilePath . $tmpFileName;



      return response()->json(array('path'=> $path), 200);

    } else {


    	return response()->json(false, 200);
    }
  }



	/*Отправка ошибок верстальщику*/
	public function errorReport(){
		$id 			= Request::input('id');
		$errors 		= Request::input('errors');

		$settings = [
		    'username' => 'Жуколов',
		    'channel' => '#accounting'
		];

		$verstalshik = ZeonWork::find($id)->programmist->oracle_user->id;

		$client = new Client('https://hooks.slack.com/services/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', $settings);
		$slack_name = User::find($verstalshik)->slack_name;

		$sitename = ZeonWork::find($id)->blank->contract->domen->name;
		
		//$errors = trim(strip_tags(str_replace('</li>', "\n", $errors)));

		$text = 'Задача "'.$sitename.'" была проверена, имеются ошибки, подробности доступны у координатора.';
		//$text .= $sitename;
		//$text .= ".\n";
		//$text .= $errors;

		$client->to('@'.$slack_name)->withIcon(':bug:')->send($text);
		return $text;
	}

	
}