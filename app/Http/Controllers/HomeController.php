<?php

namespace App\Http\Controllers;

use App\Zeon\Work as ZeonWork;
use Illuminate\Http\Request;
use App\Work;
use Auth;
use App\Oracle\User as User;
use Gate;
use DB;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void

    public function __construct()
    {
        $this->middleware('auth');
    }*/

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function filtrate($filter){
        switch ($filter){
            case 'untested':
                $status = 0;
            break;

            case 'completed':
                $status = 4;
            break;

            case 'errors':
                $status = 1;
            break;

            case 'waiting':
                $status = 3;
            break;
            case 'ready':
                $status = 2;
            break;            
        }

        return $status;
    }

    public function index($filter = null)
    {
	    $iSeeStat = 0;
        $isVerst = 0;
        $stat['today']      = Work::whereDate('created_at', date('Y-m-d'))->count(); 
        $stat['completed']  = Work::where('status_id', '=', 4)->count();
        $stat['ready']      = Work::where('status_id', '=', 2)->count();
        $stat['waiting']      = Work::where('status_id', '=', 0)->count();
        $stat['retest']      = Work::where('status_id', '=', 3)->count();
        $coords = User::where('position', 'Координатор')->get();
        if(!Auth::user()){
            return view('auth/login');
        }

        if(Auth::user()->is_checker()){
            
            if($filter != NULL)
			    $works = Work::where('status_id', $this->filtrate($filter))->orderBy('created_at', 'DESC')->simplePaginate(15);
            else
                $works = Work::orderBy('created_at', 'DESC')->simplePaginate(15);


            $iSeeStat = 1;
        }
	    else if(Auth::user()->is_coordinator()){

		    $works = \App\Zeon\Work::where('coord', '=', (int)Auth::user()->zeon_user->id)
                ->orWhere('coord', 'LIKE', (int)Auth::user()->zeon_user->id)
                ->get();
            if($filter != NULL)
                $works = Work::where('status_id', $this->filtrate($filter))->orderBy('created_at', 'DESC')->whereIn('work_id', $works->pluck('id'))->paginate(15);
            else
		        $works = Work::orderBy('created_at', 'DESC')->whereIn('work_id', $works->pluck('id'))->paginate(15);


	    }
        else if(Auth::user()->is_verstalshik()){
            $works = \App\Zeon\Work::where('prog', 'LIKE', (int)Auth::user()->zeon_user->id)
                ->orWhere('prog', 'LIKE', (int)Auth::user()->zeon_user->id)
                ->get();
            $isVerst = 1;
            if($filter != NULL)
                $works = Work::where('status_id', $this->filtrate($filter))->orderBy('created_at', 'DESC')->whereIn('work_id', $works->pluck('id'))->paginate(15);
            else
                $works = Work::orderBy('created_at', 'DESC')->whereIn('work_id', $works->pluck('id'))->paginate(15);


        }


            $i = 0;
            foreach ($works as $work){
                $workParams = $this->getVerstka_check($work->work_id);
                $userWorks = [];
                $userWorks['id'] = $work->work_id;
                $userWorks['name'] = $workParams['site'];
                $userWorks['blank'] = $workParams['blank'];
                $userWorks['domen'] = $workParams['domen'];
                $userWorks['poddomen'] = $workParams['poddomen'];
                $userWorks['type'] = $workParams['type'];
                $userWorks['verst'] = $workParams['verst'];
                $userWorks['complect'] = $workParams['complect'];
                $userWorks['status_id'] = $work->status_id;
                $userWorks['author'] = ZeonWork::find($work->work_id)->coordinator->name;
                $work->userWorks = $userWorks;
                $i++;

            }

                return view('dashboard', ['tasks' => $works, 'statistic' => $iSeeStat, 'stat' => $stat, 'isVerst' => $isVerst, 'coords' => $coords]);

			//return view('dashboard', ['tasks' => $userWorks]);
    }
	
	

	
	
	
	/**
	 * Генератор файла проверки верстки
	 * @return Redirect
	 */
	public function getVerstka_check($id){
		$work = ZeonWork::find($id);
/*
		$template = $_SERVER['DOCUMENT_ROOT']."/../storage/check_list.xml";
		$template = File::get($template);

*/
		$params = [
            'id' => $work->id,
			'site' =>  $work->blank->contract->domen->name,
			'type' => $work->type == 1 ? 'Под ключ' : 'Доработка',
			'complect' => '-',
			'domen' => $work->blank->contract->domen->name,
            'poddomen' => '',
            'verst' => $work->get_prog_name(),
			'blank' => "http://oracle.ru/blanks/docs/{$work->blank->id}.html",
			'date' => (new \DateTime())->format('d.m.Y')
		];

            //$trueDomen = $work->blank->contract->domen->name;
		if( !is_null($work->blank->icomplect) && $work->type == 1 )
			$params['complect'] = $work->blank->icomplect->name;

		if( !empty($work->cms) && mb_strlen($work->cms) > 5 )
			$params['poddomen'] = $work->cms;
		
		
		return $params;
		/*

		if( $work->type == 1 )
			$name = "Проверка качества верстки по проекту {$params['site']}";
		else
			$name = "Проверка качества выполнения доработки по проекту {$params['site']} ({$work->blank->contract_sum})";
		$file_name = "/storage/check/{$name}.doc";
				foreach( $params as $key => $val )
					$template = str_replace('{'.$key.'}', $val, $templat;
		/*
				file_put_contents( $_SERVER['DOCUMENT_ROOT'].$file_name , $template );
				return redirect($file_name);
				return response()->file($file_name, [['content-type'=>'application/vnd.openxmlformats-officedocument.wordprocessingml.document']]);
				return Response::make($template, 200, array('content-type'=>'application/vnd.openxmlformats-officedocument.wordprocessingml.document'));
		*/
	}


    public function getWork(){
        $work = Work::find($_REQUEST['id']);
        if( is_null($work) )
            return 'Нет нихрена такой работы в зеоне!';

        $task = new Task();
            $task->work()->associate($work);
            $task->status()->associate( Status::find(1) );
        $task->save();
    }
}
