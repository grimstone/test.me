<?php

namespace App;

use App\Oracle\User;
use App\Zeon\Work;
use Illuminate\Database\Eloquent\Model;

class Task extends Model{

	public function work(){
	    return $this->belongsTo(Work::class);
	}

	public function status(){
	    return $this->belongsTo(Status::class);
	}
}
