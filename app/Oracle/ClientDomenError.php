<?php

namespace App\Oracle;

use App\CitronModel as Model;

class ClientDomenError extends Model{

	protected $connection = 'oracle';
	protected $table = 'ctrl_client_domen_errors';
	protected $dates = ['date_add', 'date_end', 'date_on_moderate'];
	public $timestamps = false;

	public function domen(){
		return $this->belongsTo(ClientDomen::class, 'domen_id');
	}

	public function client(){
		return $this->belongsTo(Client::class);
	}
}
