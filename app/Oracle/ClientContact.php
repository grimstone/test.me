<?php

namespace App\Oracle;

use App\CitronModel as Model;

class ClientContact extends Model{

	protected $connection = 'oracle';
	protected $table = 'ctrl_client_contacts';

	public function client(){
		return $this->belongsTo('App\Oracle\Client');
	}

	public function domen(){
		return $this->belongsTo('App\Oracle\ClientDomen');
	}
}
