<?php

namespace App\Oracle;

use App\CitronModel as Model;

class BlankFile extends Model{

	protected $connection = 'oracle';
	protected $table = 'ctrl_client_domen_blanks_files';
}
