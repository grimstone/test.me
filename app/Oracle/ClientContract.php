<?php

namespace App\Oracle;

use App\CitronModel as Model;

/**
 * Class ClientContract
 * @package App\Oracle
 */
class ClientContract extends Model{

	/**
	 * @var string
	 */
	protected $connection = 'oracle';
	/**
	 * @var string
	 */
	protected $table = 'ctrl_client_contracts';

	/**
	 * Возвращает бланки по данному договору
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function blanks(){
		return $this->hasMany('App\Oracle\Blank', 'contract_id');
	}

	/**
	 * Возвращает клиента заключившего договор
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function client(){
		return $this->belongsTo('App\Oracle\Client');
	}

	/**
	 * Возвращает домен к которому привязан договор
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function domen(){
		return $this->belongsTo('App\Oracle\ClientDomen');
	}

	/**
	 * Возвращает компанию с которой клиент заключил данный договор
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function company(){
		return $this->belongsTo('App\Oplata\Company');
	}
}
