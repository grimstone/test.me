<?php
/**
 * Created by PhpStorm.
 * User: prog2
 * Date: 06.06.2016
 * Time: 15:00
 */

namespace App\Oracle;

use \Auth as Auth;

/**
 * Class Helper
 * @package App\Zeon\Timeline
 */
class Helper{
	/**
	 * Возвращает все рабочие дни между датами
	 * @param \DateTime $date_start Дата начала
	 * @param \DateTime $date_end Дата окончания
	 * @return array
	 */
	static public function getAllWorkDaysBetweenDates( $date_start, $date_end ){
		$date_start = clone $date_start;
		$date_end = clone $date_end;
		$return = [];

		while( $date_start <= $date_end ){

			if( Calendar::isWorkDay($date_start) )
				$return[] = clone $date_start;

			$date_start->modify('+1 day');
		}
		return $return;
	}

	/**
	 * Формирует одномерный массив с рабочими часами
	 * @return array
	 */
	static public function getAllWorkHours(){
		$return = [];
		$start = Calendar::$work_start_hour;
		while( $start < Calendar::$work_end_hour ){
			if( $start != Calendar::$work_exclude_hour )
				$return[] = $start;
			$start++;
		}
		return $return;
	}

	/**
	 * Возвращает сокращенное название дня недели
	 * @param $day Сам день
	 * @return string
	 */
	static public function getWeekDayName( $day ){
		$arr = [ 'Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб' ];
		return $arr[ $day->format('w') ];
	}

	/**
	 * Ответ с ошибкой
	 * @param $error
	 * @return mixed
	 */
	static public function sendError( $error ){
		return json_encode([
			'success' => 0,
			'error' => $error
		]);
	}

	/**
	 * Хороший ответ от сервера
	 * @param mixed $text
	 * @return mixed
	 */
	static public function sendGood( $text = ''){
		return json_encode([
			'success' => 1,
			'body' => $text
		]);
	}

	/**
	 * Возвращает кол-во дней, часов, минут из кол-ва минут
	 * @param int $minutes
	 * @return string
	 */
	static public function minutesToDaysHoursMinutes( $minutes ){

		$minus = $minutes >= 0 ? '' : '-';
		$minutes = abs($minutes);

		$days = intval( $minutes / 480 );
		$hours = intval( $minutes % 480 / 60 );
		$minutes = $minutes % 60;
		$str = '';
		if( $days )
			$str .= $days.' '.self::plur($days, 'день', 'дня', 'дней').' ';
		if( $hours )
			$str .= $hours.' '.self::plur($hours, 'час', 'часа', 'часов').' ';
		if( $minutes )
			$str .= $minutes.' '.self::plur($minutes, 'минута', 'минуты', 'минут');
		return $minus.$str;
	}

	static public function dateEngToRus( \DateTime $date ){
		$date = clone $date;
		return $date->format('H:i d.m.Y');
	}

	/**
	 * Склоняет слова
	 * @param $number
	 * @param $one
	 * @param $two
	 * @param $five
	 * @return mixed
	 */
	static public function plur($number, $one, $two, $five) {
		if (($number - $number % 10) % 100 != 10) {
			if ($number % 10 == 1) {
				$result = $one;
			} elseif ($number % 10 >= 2 && $number % 10 <= 4) {
				$result = $two;
			} else {
				$result = $five;
			}
		} else {
			$result = $five;
		}
		return $result;
	}

	/**
	 * Превращаем массив в DIVную строку
	 * @param mixed $array
	 * @return string
	 */
	public static function arrayToHtmlLines($array):string {
	    $str = '';
		foreach( $array as $item ){
			$str .= "<div>{$item}</div>";
		}
		return $str;
	}
}