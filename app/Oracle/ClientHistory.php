<?php

namespace App\Oracle;

use App\CitronModel as Model;

class ClientHistory extends Model{

	protected $connection = 'oracle';
	protected $table = 'ctrl_client_history';
	protected $dates = ['date'];
	public $timestamps = false;

	public function client(){
		return $this->belongsTo(Client::class);
	}

	public function user(){
		return $this->belongsTo(User::class);
	}
}
