<?php

namespace App\Oracle;

use Illuminate\Database\Eloquent\Collection;
use App\CitronModel as Model;

/**
 * Class Calendar
 * @package App\Oracle
 */
class Calendar extends Model{

	/**
	 * @var array
	 */
	protected $dates = ['date'];
	/**
	 * @var string
	 */
	protected $connection = 'oracle';
	/**
	 * @var string
	 */
	protected $table = 'ctrl_calendar';
	/**
	 * @var int
	 */
	static public $work_start_hour = 10;
	/**
	 * @var int
	 */
	static public $work_end_hour = 19;
	/**
	 * @var int
	 */
	static public $work_exclude_hour = 14;

	/**
	 * Высчитывает точную минуту, когда наступит плюс Х рабочих минут от какой-то даты
	 * @param \DateTime $start_time
	 * @param $minutes
	 * @return \DateTime
	 */
	static public function getPlusWorkMinutes( $start_time, $minutes ){
		$start_time = clone $start_time;
		// Защита от начала работы после начала раб дня
		if( $start_time->format('H') >= self::$work_end_hour )
			$start_time = self::getNextWorkDay( $start_time );
		// Пока не найдем день, в котором раб часы заканчиваются - ищем этот день
		while( self::thisMoreThanDay($start_time, $minutes) ){
			$minutesBeforeDayEnd = self::getMinutesBeforeDayEnd( $start_time );
			$minutes = $minutes - $minutesBeforeDayEnd;
			$start_time = self::getNextWorkDay( $start_time );
		}
		// Если кол-во минут меньше, чем осталось до конца дня - возвращаем дату за плюсом этого кол-ва
		$minutes = self::minutesFixForExdentedHour( $start_time, $minutes);
		$start_time->modify("+{$minutes} minutes");

		if( $start_time->format('H') == self::$work_exclude_hour )
			$start_time->modify("+1 hour");

		if( $start_time->format('H') >= self::$work_end_hour )
			$start_time = self::getNextWorkDay( $start_time );

		return $start_time;

	}

	/**
	 * Получаем +Х рабочих дней от даты
	 * @param \DateTime $date
	 * @param $count
	 * @return \DateTime|null
	 */
	public static function getPlusWorkDays( \DateTime $date, $count ){
	    while( $count > 0 ){
		    $date = self::getNextWorkDay($date);
		    $count--;
	    }
	    return $date;
	}

	/**
	 * Получаем -Х рабочих дней
	 * @param \DateTime $date
	 * @param $count
	 * @return \DateTime|null
	 */
	public static function getMinusWorkDays( \DateTime $date, $count ){
		while( $count > 0 ){
			$date = self::getPrevWorkDay($date);
			$count--;
		}
		return $date;
	}

	/**
	 * Возвращает начало следующего рабочего дня
	 * @param \DateTime $this_day
	 * @return \DateTime |null
	 */
	static public function getNextWorkDay( $this_day ){
		$this_day = clone $this_day;
		// Возвратик даты
		while( true ){
			// Берем след день
			$this_day->modify('+1 day');
			// Ищем запись по этому дню в базе
			$this_day_on_base = self::whereDate('date', '=', $this_day->format('Y-m-d'))->first();
			// Если небыло записи о дне - судим о нем по дню недели
			if( is_null($this_day_on_base) ){
				if( $this_day->format('w') > 0 && $this_day->format('w') < 6  ){
					//dd(1, $this_day->setTime(self::$work_start_hour, 0));
					return $this_day->setTime(self::$work_start_hour, 0);
				}
			}else{
				if( in_array($this_day_on_base->status, [1, 3]) ){
					//dd(2, $this_day->setTime(self::$work_start_hour, 0));
					return $this_day->setTime(self::$work_start_hour, 0);
				}
			}
		}
	}

	/**
	 * Возвращает конец предыдущего рабочего дня
	 * @param \DateTime $this_day
	 * @return \DateTime |null
	 */
	static public function getPrevWorkDay( $this_day ){
		$this_day = clone $this_day;
		// Возвратик даты
		while( true ){
			// Берем след день
			$this_day->modify("-1 day");
			// Ищем запись по этому дню в базе
			$this_day_on_base = self::whereDate('date', '=', $this_day->format('Y-m-d'))->first();
			// Если небыло записи о дне - судим о нем по дню недели
			if( is_null($this_day_on_base) ){
				if( $this_day->format('w') > 0 && $this_day->format('w') < 6  ){
					//dd(1, $this_day->setTime(self::$work_start_hour, 0));
					return $this_day->setTime(self::$work_end_hour, 0);
				}
			}else{
				if( in_array($this_day_on_base->status, [1, 3]) ){
					//dd(2, $this_day->setTime(self::$work_start_hour, 0));
					return $this_day->setTime(self::$work_end_hour, 0);
				}
			}
		}
	}

	/**
	 * Сравнивает количество оставшихся минут в этом дне и переданные минуты
	 * @param \DateTime $day
	 * @param $minutes
	 * @param  bool $back_mode Считаем не до конца дня, а до начала
	 * @return bool
	 */
	static public function thisMoreThanDay( $day, $minutes, $back_mode = false){
		return $minutes > self::getMinutesBeforeDayEnd( $day, $back_mode );
	}

	/**
	 * Высчитывает количество минут до конца или начала данного дня
	 * @param \DateTime $day
	 * @param  bool $back_mode Считаем не до конца дня, а до начала
	 * @return int
	 */
	static public function getMinutesBeforeDayEnd( $day, $back_mode = false ){
		$day = clone $day;
		$hour = $back_mode ? self::$work_start_hour : self::$work_end_hour;
		$next_date = new \DateTime( $day->format('Y-m-d').' '.$hour.':00:00' );
		$interval = $day->diff($next_date);
		//dd($interval, $day, $next_date, $back_mode);
		// Выкидываем час обеда
		if( (!$back_mode && $day->format('H') <= self::$work_exclude_hour) || ($back_mode && $day->format('H') >= self::$work_exclude_hour) )
			return $interval->h * 60 + $interval->i - 60;
		else
			return $interval->h * 60 + $interval->i;
	}

	/**
	 * Фиксирует рабочее время с поправкой на час обеда
	 * @param \DateTime $day
	 * @param int $minutes
	 * @return int
	 */
	static public function minutesFixForExdentedHour( $day, $minutes ){
		$day = clone $day;

		if( $day->format('H') > self::$work_exclude_hour )
			return $minutes;

		$interval = $day->diff(new \DateTime( $day->format('Y-m-d').' '.self::$work_exclude_hour.':00:00' ) );
		$interval = $interval->h * 60 + $interval->i;
		if( $minutes < $interval )
			return $minutes;
		else
			return $minutes + 60;
	}

	/**
	 * Считает сколько рабочих часов между двумя датами
	 * @param \DateTime $date_start
	 * @param \DateTime $date_end
	 * @return bool
	 */
	static public function getWorkMinutesBetweenDates( $date_start, $date_end ){
		$date_start = clone $date_start;
		$date_end = clone $date_end;
		/**
		 * Если нам передали не рабочии дни - работим их
		 */
		if( !self::isWorkDay($date_start) ){

			$h = $date_start->format( 'H' );
			$i = $date_start->format( 'i' );
			// Если дата_старта меньше даты_конца - ищем след день, иначе предыдущий
			if( $date_start < $date_end )
				$date_start = self::getNextWorkDay( $date_start );
			else
				$date_start = self::getPrevWorkDay( $date_start );
			$date_start->setTime($h, $i);
		}

		if( !self::isWorkDay($date_end) ){

			$h = $date_end->format( 'H' );
			$i = $date_end->format( 'i' );

			if( $date_start < $date_end )
				$date_end = self::getNextWorkDay( $date_end );
			else
				$date_end = self::getPrevWorkDay( $date_end );
			$date_end->setTime($h, $i);
		}
		// Переменная под возврат
		$between = 0;
		// ЗАщита от корявых дат
		if( $date_end < $date_start ){
			while( $date_start >= $date_end ){
				// Если наш цикл дошел до равности дат
				if( $date_start->format( 'Y-m-d' ) == $date_end->format( 'Y-m-d' ) ){
					$interval = $date_start->diff( $date_end );
					//dd($interval, $date_start, $date_end, $between);
					$interval = $interval->h * 60 + $interval->i;
					if( $date_start->format( 'H' ) >= self::$work_exclude_hour && $date_end->format( 'H' ) < self::$work_exclude_hour )
						$interval = $interval - 60;
					// Пока цикл не дошел до равности дат - считаем по оставшимся раб часам в дне
				}else{
					$interval = self::getMinutesBeforeDayEnd( $date_start, true );
				}
				$between += $interval;
				// Не только лишь все дни могут идти в предыдущий день. Точнее все могут, но не лишь все.
				$date_start = self::getPrevWorkDay( $date_start );
			}
		}else if( $date_end > $date_start ){
			while( $date_start <= $date_end ){
				// Если наш цикл дошел до равности дат
				if( $date_start->format( 'Y-m-d' ) == $date_end->format( 'Y-m-d' ) ){
					$interval = $date_start->diff( $date_end );
					$interval = $interval->h * 60 + $interval->i;
					if( $date_start->format( 'H' ) <= self::$work_exclude_hour && $date_end->format( 'H' ) > self::$work_exclude_hour )
						$interval = $interval - 60;
					// Пока цикл не дошел до равности дат - считаем по оставшимся раб часам в дне
				}else{
					$interval = self::getMinutesBeforeDayEnd( $date_start );
				}
				$between += $interval;
				// Не только лишь все дни могут идти в следующий день. Точнее все могут, но не лишь все.
				$date_start = self::getNextWorkDay( $date_start );
			}
		}else
			return 0;

		return $between;
	}

	/**
	 * Дает знат рабочий ли этот день
	 * @param \Datetime $day
	 * @return bool
	 */
	static public function isWorkDay( $day ){
		$this_day_on_base = self::whereDate('date', '=', $day->format('Y-m-d'))->first();
		if( is_null($this_day_on_base) || $this_day_on_base->status == 2 )
			return false;
		return true;
	}

	/**
	 * Высчитывает точную минуту, когда наступит минус Х рабочих минут от какой-то даты
	 * @param \DateTime $start_time
	 * @param $minutes
	 * @return \DateTime
	 */
	static public function getMinusWorkMinutes( $start_time, $minutes){
		$start_time = clone $start_time;
		// Защита от неадекватных данных
		if( $start_time->format('H') < self::$work_start_hour )
			$start_time = self::getPrevWorkDay( $start_time );
		// Пока не найдем день, в котором раб часы заканчиваются - ищем этот день
		while( self::thisMoreThanDay($start_time, $minutes) ){
			$minutesBeforeDayEnd = self::getMinutesBeforeDayEnd( $start_time );
			$minutes = $minutes - $minutesBeforeDayEnd;
			$start_time = self::getPrevWorkDay( $start_time );
		}
		// Если кол-во минут меньше, чем осталось до конца дня - возвращаем дату за плюсом этого кол-ва
		$minutes = self::minutesFixForExdentedHour( $start_time, $minutes);
		$start_time->modify("+{$minutes} minutes");
		if( $start_time->format('H') == self::$work_exclude_hour )
			$start_time->modify("+1 hour");
		return $start_time;

	}

	/**
	 * Возвращает информацию о рабочем времени в месяце
	 * @param \DateTime $date
	 * @return array
	 */
	static public function getMonthWorkInfo(\DateTime $date){
		$date = clone $date;

		$date_start = clone $date;
		$date_start->modify("last day of last month");
		$date_start = self::getNextWorkDay($date_start);

		$date_end = clone $date;
		$date_end->modify("first last day of next month");
		$date_end = self::getPrevWorkDay($date_end);

		$return = [];
		// Не завершено написание ф-ции
		return $return;
	}

	static public function getMinusWorkDay( $this_day, $days ){
		$this_day = clone $this_day;
		$minused = 0;
		// Возвратик даты
		while( true ){
			// Берем след день
			$this_day->modify("-1 day");
			// Ищем запись по этому дню в базе
			$this_day_on_base = self::whereDate('date', '=', $this_day->format('Y-m-d'))->first();
			// Если небыло записи о дне - судим о нем по дню недели
			if( is_null($this_day_on_base) ){
				if( $this_day->format('w') > 0 && $this_day->format('w') < 6  ){
					$minused++;

					if( $minused >= $days )
						return $this_day->setTime(0, 0);

					continue;
				}
			}else{
				if( in_array($this_day_on_base->status, [1, 3]) ){
					$minused++;

					if( $minused >= $days )
						return $this_day->setTime(0, 0);

					continue;
				}
			}
		}
	}

	/**
	 * Возвращает месяц по-русски
	 * @param \DateTime $date
	 * @return string
	 */
	public static function getMonthName(\DateTime $date):string {
		// Массив с месяцами
		$months = ['Январь','Февраль','Март','Апрель','Май','Июнь','Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'];
		// Берем номер месяца начиная с 0
		$month_num = intval($date->format('m'))-1;
		// Защита от некорректных данных
		if( isset($months[$month_num]) )
			return $months[$month_num];
		else
			return 'Хренабрь';
	}

	/**
	 * Возвращает массив дней месяца
	 * @param \DateTime $date
	 * @return Collection
	 */
	public static function getMonthDays(\DateTime $date){
		// На всякий случай сбрасывает часы и минуты на 0
		$date = new \DateTime($date->format('Y-m-d'));
		// Массив под возврат
		$days = new Collection();
		// Текущий день в цикле. Начинаем с 1ого дня месяца
		$current = (clone $date)->modify('first day of this month');
		// Посл день месяца
		$end = (clone $date)->modify('last day of this month');
		// Сегодня
		$today = (new \DateTime())->setTime(0, 0, 0);
		// День за днем
		while( $current <= $end ){
			// Сам день
			$day = new \stdClass();
			// Порядковый номер дня в формате с ведущим нулем
			$day->num = $current->format('d');
			// Полная дата
			$day->date = $current->format('Y-m-d');
			// Рабочий ли день
			$day->is_working = self::isWorkDay($current);
			// Текущий ли день
			$day->is_current = $current == $today;
			// Прошедший ли день
			$day->is_past = $current < $today;
			// Будущий ли день
			$day->is_future = $current > $today;
			// Вносим день в массив дней
			$days->add($day);
			// Мы одни из не много лишь тех, кто может смотреть в завтрашний день.
			$current->modify('+1 day')->setTime(0, 0, 0);
		}
		return $days;
	}
}
