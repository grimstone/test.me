<?php

namespace App\Oracle;

use App\CitronModel as Model;
use App\Zarplata\ZarplataHelper;

/**
 * Class Blago
 * @package App\Oracle
 */
class Blago extends Model{
	protected $dates = ['date'];
	private $prem_size = 500;
	protected $table = 'ctrl_client_blago';
	protected $connection = 'oracle';

	public function client(){
		return $this->belongsTo(Client::class);
	}

	public function domen(){
		return $this->belongsTo(ClientDomen::class, 'domen_id');
	}

	public function users(){
		return $this->belongsToMany(User::class, 'ctrl_client_blago_users');
	}

	public function oneUserPrem(){
		return ZarplataHelper::showSum( $this->prem_size / $this->users->count() );
	}
}
