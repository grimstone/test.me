<?php

namespace App\Oracle;

use App\CitronModel as Model;

/**
 * Class Blank
 * @package App\Oracle
 */
class HostingPrice extends Model{
	/**
	 * @var string
	 */
	protected $connection = 'oracle';
	/**
	 * @var string
	 */
	protected $table = 'ctrl_hosting_prices';
}
