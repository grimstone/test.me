<?php

namespace App\Oracle;

use App\RememberMe;
use App\Task;
use Illuminate\Foundation\Auth\User as Authenticatable;


class User extends Authenticatable{

	use RememberMe;

	protected $connection = 'oracle';
	protected $table = 'ctrl_users';

	/**
	 * Все счета выставленные юзером
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function payments(){
	    return $this->hasMany(Payment::class, 'manager_id');
	}

	/**
	 * Связь с пользователем зеона
	 * @return mixed
	 */
	public function zeon_user(){
		return $this->hasOne('App\Zeon\User', 'id', 'system_id');
	}
	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
	 */
	public function rights(){
		return $this->belongsToMany('App\Oracle\UserRight', 'ctrl_user_rights_xref', 'user_id', 'right_id');
	}

	/**
	 * Проверка наличия прав по ид
	 * @param $right_id
	 * @return bool
	 */
	public function hasRight($right_id){
		return !$this->rights->filter(function($right) use ($right_id){
			return $right->id == $right_id;
		})->isEmpty();
	}

	/**
	 * Благодарности
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
	 */
	public function blagos(){
		return $this->belongsToMany(Blago::class, 'ctrl_client_blago_users');
	}


	/**
	 * Админ ли я или тварь дрожащая
	 * @return bool
	 */
	public function is_admin(){
		if( $this->group == 1 || $this->id == 61 || $this->id == 174 )
			return true;

		return false;
	}

	/**
	 * Бланки пользователя
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function blanks(){
		return $this->hasMany(Blank::class);
	}

	/**
	 * Заплатка под авторизацию
	 * @return mixed
	 */
	public function getAuthPassword(){
		return \Hash::make($this->password);
	}

	/**
	 * Определяем - является ли юзер проверяющим верстку
	 * @return bool
	 */
	public function is_checker() : bool {
		if( $this->id == 61 || $this->id == 200 || $this->id == 1 || $this->id == 183)
			return true;

		return false;
	}

	/**
	 * Определяем - является ли юзер координтаором
	 * @return bool
	 */
	public function is_coordinator() : bool {

		if( $this->system == 'zeon' && !is_null($this->zeon_user) && $this->zeon_user->hasRight(1) || $this->id == 167)
			return true;

		return false;
	}

	/**
	 * Определяем - является ли юзер верстальщиком
	 * @return bool
	 */
	public function is_verstalshik() : bool {

		if( !is_null($this->zeon_user) && $this->zeon_user->hasRight(2) )
			return true;

		return false;
	}

	/**
	 * Все задачи пользователя
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function tasks(){
	    return $this->hasMany(Task::class);
	}
}
