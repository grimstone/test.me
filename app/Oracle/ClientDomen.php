<?php

namespace App\Oracle;

use App\Snimalka\SnimalkaProject;
use App\CitronModel as Model;

/**
 * Клиентские домены в базе
 * Class ClientDomen
 * @package App\Oracle
 */
class ClientDomen extends Model{

	protected $connection = 'oracle';
	protected $table = 'ctrl_client_domens';
	protected $dates = ['hosting_end', 'domen_end', 'su_end', 'hosting_off_date'];

	/**
	 * Клиент
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function client(){
		return $this->belongsTo('App\Oracle\Client');
	}

	/**
	 * Балнки заказа
	 * @return \Illuminate\Database\Eloquent\Relations\HasManyThrough
	 */
	public function blanks(){
		return $this->hasManyThrough('App\Oracle\Blank', 'App\Oracle\ClientContract', 'domen_id', 'contract_id');
	}

	/**
	 * Тариф хостинга
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function hosting_tarif(){
		return $this->belongsTo(HostingPrice::class, 'hosting_price');
	}

	/**
	 * План хостинг\домен\су
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function plans(){
		return $this->hasMany(ClientDomenPlan::class, 'domen_id');
	}

	/**
	 * Ошибки ОРК
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function errors(){
		return $this->hasMany(ClientDomenError::class, 'domen_id');
	}

	/**
	 * Проекты на продвижении
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function seoProjects(){
	    return $this->hasMany(SnimalkaProject::class, 'domen_id');
	}
}
