<?php

namespace App\Oracle;

use App\CitronModel as Model;

class ClientDomenPlan extends Model{

	protected $connection = 'oracle';
	protected $table = 'ctrl_month_plan';
	protected $dates = ['date', 'hosting_end', 'domen_end', 'su_end', 'hosting_3day', 'domen_3day', 'su_3day', 'date_start'];

	public function domen(){
		return $this->belongsTo(ClientDomen::class, 'domen_id');
	}
}
