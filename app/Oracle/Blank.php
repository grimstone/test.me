<?php

namespace App\Oracle;

use App\CitronModel as Model;
use App\Oplata\Order;

/**
 * Class Blank
 * @package App\Oracle
 */
class Blank extends Model{
	protected $dates = ['date_zeon_add', 'date_add', 'date_zeon_end', 'seo_date', 'update'];
	/**
	 * @var array
	 */
	private $elit_complects = [5, 14, 21];
	/**
	 * @var array
	 */
	private $elit_modules = [11, 312, 330, 695, 798, 1304, 4178];
	/**
	 * @var array
	 */
	private $is_catalog_modules = [86, 149];
	/**
	 * @var string
	 */
	protected $connection = 'oracle';
	/**
	 * @var string
	 */
	protected $table = 'ctrl_client_domen_blanks';

	/**
	 * Возвращает задачу в зеоне по данному бланку
	 * @return mixed
	 */
	public function work(){
		return $this->hasOne('App\Zeon\Work', 'base_id')->where('base', 'blanks');
	}

	/**
	 * Возвращает договор по которому создан бланк
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function contract(){
		return $this->belongsTo('App\Oracle\ClientContract');
	}

	/**
	 * Возвращает создателя бланка
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function user(){
		return $this->belongsTo(User::class);
	}

	/**
	 * Возвращает клиента бланка
	 * @return mixed
	 */
	public function get_client(){
		return $this->contract->client;
	}

	/**
	 * Возвращает основной домена бланка
	 * @return mixed
	 */
	public function get_domen(){
		return $this->contract->domen;
	}

	/**
	 * Возвращает список допмодулей бланка
	 * @return mixed
	 */
	public function modules(){
		return $this->belongsToMany('App\Zeon\Module', 'matrix.ctrl_client_domen_blanks_modules_xref')->withPivot(['coord', 'diz_prem', 'desc', 'count', 'price'])->orderBy('sort');
	}

	/**
	 * Возвращает список файлов бланка
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function files(){
		return $this->hasMany('App\Oracle\BlankFile');
	}

	/**
	 * Возвращает список доменов бланка
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function domens(){
		return $this->hasMany('App\Oracle\BlankDomen');
	}

	/**
	 * Возвращает комплект сайта
	 * @return \Illuminate\Database\Eloquent\Relations\HasOne
	 */
	public function icomplect(){
		return $this->hasOne('App\Zeon\Complect', 'id', 'complect');
	}

	/**
	 * Возвращает комплект магазина
	 * @return \Illuminate\Database\Eloquent\Relations\HasOne
	 */
	public function ishop(){
		return $this->hasOne('App\Zeon\Shop', 'id', 'shop');
	}

	/**
	 * Возвращает список оплат по бланку
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public  function orders(){
		return $this->hasMany(Order::class);
	}

	/**
	 * Возвращает элитность бланка для дизайнера\верстальзика
	 * @return bool
	 */
	public function is_elit(){
		if( in_array($this->complect, $this->elit_complects) )
			return true;
		foreach( $this->modules as $module )
			if( in_array($module['id'], $this->elit_modules) )
				return true;
		
		return false;
	}

	public function coord(){
		return $this->belongsTo(\App\Zeon\User::class, 'blablacoord');
	}

	/**
	 * Возвращает магазинность бланка для дизайнера\верстальзика
	 * @return bool
	 */
	public function is_shop(){
		if( !is_null($this->ishop) )
			return true;

		foreach( $this->modules as $module )
			if( in_array($module['id'], $this->is_catalog_modules) )
				return true;

		return false;
	}

	public function elit_name(){
		return $this->is_elit() ? 'Элит' : "Обычный";
	}

	public function elit_eng_name(){
		return $this->is_elit() ? 'elit' : "norm";
	}

	public function calcDiscount(){
		if( !$this->contract_sum )
			return 100;
		return $this->discount / (($this->contract_sum + $this->discount) / 100);
	}
}
