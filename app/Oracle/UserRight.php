<?php

namespace App\Oracle;

use App\CitronModel as Model;

class UserRight extends Model{

	protected $connection = 'oracle';
	protected $table = 'ctrl_user_rights';

	public function users(){
		return $this->belongsToMany('App\Zeon\User', 'ctrl_user_rights_xref', 'user_id', 'right_id');
	}
}
