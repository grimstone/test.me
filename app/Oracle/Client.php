<?php

namespace App\Oracle;

use App\RememberMe;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Client
 * @package App\Oracle
 */
class Client extends Model{

	use RememberMe;

	/**
	 * @var string
	 */
	protected $connection = 'oracle';
	/**
	 * @var string
	 */
	protected $table = 'ctrl_clients';

	/**
	 * Возвращает список контрактов клиента
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function contracts(){
		return $this->hasMany('App\Oracle\ClientContract');
	}

	/**
	 * Возвращает список сонтактов клиента
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function contacts(){
		return $this->hasMany('App\Oracle\ClientContact');
	}

	/**
	 * Возвращает список доменов клиента
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function domens(){
		return $this->hasMany('App\Oracle\ClientDomen');
	}

	/**
	 * Возвращает менеджера по хостингу клиента
	 * @return \Illuminate\Database\Eloquent\Relations\HasOne
	 */
	public function manager(){
		return $this->hasOne('App\Oracle\User', 'id', 'manager_id');
	}

	/**
	 * Возвращает список бланков клиента
	 * @return \Illuminate\Database\Eloquent\Relations\HasManyThrough
	 */
	public function blanks(){
		return $this->hasManyThrough('App\Oracle\Blank', 'App\Oracle\ClientContract', 'client_id', 'contract_id');
	}

	/**
	 * Возвращает все записи в истории по юзеру
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function histories(){
		return $this->hasMany(ClientHistory::class);
	}

	/**
	 * Ошибки орк
	 * @return mixed
	 */
	public function errors(){
		return $this->hasMany(ClientDomenError::class);
	}

	/**
	 * Все прокты на продвижении у клиента
	 * @param bool $with_off Включая отключенные проекты или нет
	 * @return Collection
	 */
	public function getSeoProjects( $with_off = false ){
	    $projects = new Collection();
		foreach( $this->domens as $domen ){
			$seoProjects = $with_off ? $domen->seoProjects : $domen->seoProjects()->where('is_off', 0)->get();
			foreach( $seoProjects as $seoProject )
				$projects->add($seoProject);
		}

		return $projects;
	}

	/**
	 * Является ли клиент VIP по продвижению
	 * @return bool
	 */
	public function isSeoVip() : bool {
		return $this->getSumSeoBudget() >= config('snimalka.vip');
	}

	/**
	 * Возвращает сумму бюджетов проектов на продвижении
	 * @return int
	 */
	public function getSumSeoBudget() : int {
		$sum = 0;
		foreach( $this->getSeoProjects() as $project )
			$sum += $project->getBudget();
		return $sum;
	}

	/**
	 * Возвращает уникальные контактные данные
	 * @param string $what
	 * @return \Illuminate\Support\Collection|static
	 */
	public function getUniqueContactParam($what = 'email'){
	    $params = new Collection();
		$params->add(["{$what}" => $this->{$what}]);

		foreach( $this->contacts as $contact )
			if( !empty($contact->{$what}) )
				$params->add(["{$what}" => $contact->{$what}]);

		return $params->unique("{$what}")->pluck("{$what}");
	}
}
