<?php
/**
 * Created by PhpStorm.
 * User: prog2
 * Date: 21.11.2016
 * Time: 11:27
 */

namespace App;


use Watson\Rememberable\Rememberable;

trait RememberMe{
	use Rememberable;

	protected $rememberFor = -1;
	protected $rememberCacheTag = '';

	public static function boot(){

		parent::boot();

		static::updating(function($model){
			$model->flushCache();
		});

		static::deleting(function($model){
			$model->flushCache();
		});
	}

	public function __construct(){
		$this->rememberCacheTag = get_called_class();
		parent::__construct();
	}
}